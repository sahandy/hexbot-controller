package com.android.tum.de.hexbotcontroller.controller;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.tum.de.hexbotcontroller.comm.HexTalker;
import com.android.tum.de.hexbotcontroller.comm.RobotBluetoothManager;
import com.android.tum.de.hexbotcontroller.log.*;

import com.android.tum.de.hexbotcontroller.R;
import com.android.tum.de.hexbotcontroller.util.HexAutoPilot;
import com.android.tum.de.hexbotcontroller.util.HexLocation;
import com.android.tum.de.hexbotcontroller.util.HexSensor;
import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;


public class MainActivity extends ActionBarActivity implements
        ConnectToServerDialogFragment.ConnectToServerDialogListener {

    // Debugging
    public static final String TAG = "MainActivity";

    // Message types coming from RobotBluetoothManager Handler
    public static final int BT_MESSAGE_STATE_CHANGE = 1;
    public static final int BT_MESSAGE_READ = 2;
    public static final int BT_MESSAGE_WRITE = 3;
    public static final int BT_MESSAGE_DEVICE_NAME = 4;
    public static final int BT_MESSAGE_TOAST = 5;

    // Key names coming from RobotBluetoothManager Handler
    public static final String BT_DEVICE_NAME = "device_name";
    public static final String BT_TOAST = "toast";

    /*
    * INTENT REQUEST CODES
    */
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_MANUAL_NAVIGATION = 3;

    /*
    * HEX TALKER CODES
    */
    public static final int HEX_MESSAGE_REQUEST = 10;
    public static final int HEX_MESSAGE_RESPONSE = 11;

    private BluetoothAdapter mBluetoothAdapter;
    public static RobotBluetoothManager mRobotBTManager;

    // UI elements
    private ToggleButton btnBluetooth;
    private static ToggleButton btnRobotMaster;
    private Button btnConnect, btnDisconnect;
    private TextView txtConnectionState;

    public static HexLocation mHexLocation;
    protected static HexAutoPilot mHexAp;
    public static HexSensor mHexSensor;
//    private Location mLastLocation;

    /* For Testing purpose, checks whether the App is capable of sending a string to the other device
	private Button testSendBtn;
	private EditText txtOut;*/
    private static TextView mLogTextView;
    // Custom Log functionality
    public static CustomLogManager logMgr;

    SharedPreferences sharedPrefs;

    // ConnectToServer DialogFragment
    ConnectToServerDialogFragment mConnectDialog;
    public static SocketHelper mSocketHelper;

    // Handler
    Handler hexHandler = new HexHandler( new WeakReference<MainActivity>(this) );

    private String mBtConnectedDeviceName;
    /*
    TEMP VALUES FOR BLUETOOTH NAVIGATION COMMANDS
     TODO handle bluetooth command preparation in a separate class
     */
    public static Byte [] fwdBtReady, bwdBtReady, leftBtReady, rightBtReady;
    private final String DURATION = ApplicationConstants.defaultCommandDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(TAG, "====== > HERE 01");

        /**
         * Initialization
         */
        // Initialization of preferences values for the case when user enters the application
        // FOR THE FIRST TIME
        PreferenceManager.setDefaultValues(getApplicationContext(), R.xml.preferences, false);

        // Get preferences values from SharedPreferences object
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        Log.i(TAG, "====== > HERE 02");
        // UI
        //Initialize the custom logging functionality
        mLogTextView = (TextView)findViewById(R.id.txtView_LogConsole);
        mLogTextView.setMovementMethod(new ScrollingMovementMethod());
        txtConnectionState = (TextView)findViewById(R.id.txtView_connection_state);
        btnConnect = (Button)findViewById(R.id.button_connect_to_controller);
        btnDisconnect = (Button)findViewById(R.id.button_disconnect_from_controller);

        Log.i(TAG, "====== > HERE 03");

        btnConnect.setEnabled(true);
        btnDisconnect.setEnabled(false);

        String temp = sharedPrefs.getString( getResources().getString(R.string.pref_logLevel_key), getResources().getString(R.string.pref_logLevel_default) );
        logMgr = new CustomLogManager(LogLevel.valueOf(temp) );
        logMgr.init();

        logMgr.d(TAG, "Entered onCreate");

        logMgr.d(TAG, "Initializing Bluetooth manager");
        mRobotBTManager = new RobotBluetoothManager(this, btHandler);
        logMgr.d(TAG, "Bluetooth manager initialized");

        /*
         Bluetooth
          */
        // Get BluetoothAdapter instance
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Check whether the device supports Bluetooth
        if(mBluetoothAdapter == null){
            Toast.makeText(this, "Bluetooth is not available on this device", Toast.LENGTH_SHORT).show();

            // Save the de.tum.android.log before exiting the application
            logMgr.writeLogToFileSD(mLogTextView.getText().toString());
            finish();
            return;
        }
        // Set up buttons
        btnBluetooth = (ToggleButton)findViewById(R.id.button_bluetooth);
        if(mBluetoothAdapter.isEnabled()){
            btnBluetooth.setChecked(true);
        } else {
            btnBluetooth.setChecked(false);
        }

        /*
        UI: Master Button init
         */
        //Set the MasterButton's initial value to deactivated
        btnRobotMaster = (ToggleButton)findViewById(R.id.button_master_switch);
        setRobotMasterBtnStatus(false);


        /*
        Socket initialization
         */
        mSocketHelper = SocketHelper.getInstance();

        /*
        *  Button Events
        */
        // connect to server
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect();
//                mConnectDialog = new ConnectToServerDialogFragment();
//                mConnectDialog.show(getFragmentManager(), "ConnectDialogFragment");
            }
        });

        /*
        *  disconnect from server
        */
        btnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO put the implementation in a separate method
                if (!mSocketHelper.disconnect()) {
                    Toast.makeText(getApplicationContext(), "error disconnecting", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "Connection to server terminated", Toast.LENGTH_SHORT).show();
                txtConnectionState.setText(R.string.txt_connection_state_disconnected);
                txtConnectionState.setTextColor(Color.RED);

                btnConnect.setEnabled(true);
                btnDisconnect.setEnabled(false);
            }
        });

        // Location Service
        mHexLocation = new HexLocation(getApplicationContext());
        mHexLocation.init();

        mHexSensor = HexSensor.getInstance();
        mHexSensor.setContext(getApplicationContext());
        mHexSensor.init();
        // Update values using data stored in the Bundle.
        //mHexLocation.updateValuesFromBundle(savedInstanceState);

        // prepare bluetooth commands
        prepareBluetoothCommands();

    }

    @Override
    protected void onResume() {
        super.onResume();
        logMgr.d(TAG, "Entered onResume");

        if(mBluetoothAdapter.isEnabled()){
            btnBluetooth.setChecked(true);
        } else {
            btnBluetooth.setChecked(false);
        }

        // connect the GoogleApiClient
        mHexLocation.connectGoogleApiClient();

        // Set KeepScreen On/Off
        boolean keepScreenOn = sharedPrefs.getBoolean( getResources().getString(R.string.pref_keep_screen_on_key), true);
        if(keepScreenOn){
            this.getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            this.getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        logMgr.d(TAG, "Entered onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logMgr.d(TAG, "Entered onStop");
        // disconnect the GoogleApiClient
        mHexLocation.disconnectGoogleApiClient();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        logMgr.d(TAG, "Entered onDestroy");
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsActivityIntent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settingsActivityIntent);
            return true;
        }
        if (id == R.id.menuVirtualJoyStick) {
            Intent virtualJoyStickIntent = new Intent(MainActivity.this, VirtualJoyStickActivity.class);
            startActivity(virtualJoyStickIntent);
            return true;
        }
        if (id == R.id.menuNearbyDevices) {
            Intent nearbyDeviceConnectIntent = new Intent(this, BluetoothDeviceList.class);
            startActivityForResult(nearbyDeviceConnectIntent, REQUEST_CONNECT_DEVICE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Connect to the Controller App
    private void connect() {
        mConnectDialog = new ConnectToServerDialogFragment();
        mConnectDialog.show(getFragmentManager(), "ConnectDialogFragment");
    }

    //
    // Button Events
    //
    public static void setRobotMasterBtnStatus(final boolean checked) {
        logMgr.d(TAG, "Set status of toggle button to: " + String.valueOf(checked));
        btnRobotMaster.post(new Runnable() {
            @Override
            public void run() {
                btnRobotMaster.setChecked(checked);
            }
        });
    }

    // Method for Bluetooth Toggle button
    public void onBtToggleClicked(View v){
        boolean on = ((ToggleButton) v).isChecked();

        if(on){
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            mBluetoothAdapter.disable();
            logMgr.w(TAG, "Bluetooth turned off by user");
        }
    }

    // Method for Robot Master Switch Toggle button
    public void onRobotMasterToggleClicked(View v) {
        // Empty...
    }

    //
    // Callbacks
    //
    /* The dialog fragment receives a reference to this Activity through the
    *  Fragment.onAttach() callback, which it uses to call the following methods
    *  defined by the NoticeDialogFragment.NoticeDialogListener interface
    */
    @Override
    public void onDialogPositiveClick(ConnectToServerDialogFragment dialog) {
        // User touched the dialog's positive button

        mSocketHelper = new SocketHelper();
        if( !mSocketHelper.connect(dialog.getServerAddress()) ) { // If connection failed...
            Toast.makeText(
                    getApplicationContext(),
                    "Invalid server address",
                    Toast.LENGTH_SHORT
            ).show();
            return;
        }
        // Connected successfully, update the connection state text
        txtConnectionState.setText(R.string.txt_connection_state_connected);
        txtConnectionState.setTextColor(Color.parseColor("#669900"));
        btnConnect.setEnabled(false);
        btnDisconnect.setEnabled(true);
        mSocketHelper.sendEvent("add user", dialog.getCliendId());
        mSocketHelper.addListener("new message",new_message);
    }

    @Override
    public void onDialogNegativeClick(ConnectToServerDialogFragment dialog) {
        // User touched the dialog's negative button
        Toast.makeText(
                getApplicationContext(),
                "Dialog: Cancelled by user",
                Toast.LENGTH_SHORT
        ).show();
    }

    Emitter.Listener new_message = new Emitter.Listener(){
        @Override
        public void call(Object... args) {
            final JSONObject data = (JSONObject) args[0];
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Toast.makeText(getApplicationContext(),
                                data.getString("message"),
                                Toast.LENGTH_SHORT)
                                .show();
                        logMgr.i(TAG, "received: " + data.getString("message"));

                        handleIncomingMessage(data.getString("message"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    };

    public void handleIncomingMessage(String msg) {
        HexTalker talker = new HexTalker(getApplicationContext(), hexHandler);

        talker.setInMessage(msg);
        talker.initializeParser();
        talker.parseIncomingMessage();

    }

    // Handler that gets information back from the HexTalker
//    private Handler hexHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            logMgr.d(TAG, "entered hexHandler");
//            logMgr.d(TAG, "msg.what: " + String.valueOf(msg.what));
//            logMgr.d(TAG, "msg.arg1: " + String.valueOf(msg.arg1));
//            switch (msg.what) {
//                case HEX_MESSAGE_REQUEST:
//                    switch (msg.arg1) {
//                        case HexTalker.CODE_LAST_LOCTATION:
//                            logMgr.d(TAG, "case LAST LOCATION");
//                            // 1. get last known location
//                            mLastLocation = mHexLocation.getLastLocation();
//                            if (mLastLocation == null) {
//                                logMgr.e(TAG, "mLastLocation == null");
//                                break;
//                            }
//                            // 2. prepare the message: Last Location
//                            HexTalker resTalker = new HexTalker(getApplicationContext());
//                            resTalker.setType(HexTalker.TYPE_RESPONSE);
//                            resTalker.setCode(HexTalker.CODE_LAST_LOCTATION);
//                            Bundle b = new Bundle();
//                            b.putString(
//                                    ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE,
//                                    String.valueOf(mLastLocation.getLatitude()) );
//                            b.putString(
//                                    ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE,
//                                    String.valueOf(mLastLocation.getLongitude()) );
//                            resTalker.setExtra(HexTalker.EXTRA_LOCATION, b);
//
//                            resTalker.formatOutMessage();
//                            // 3. send the message back to client
//                            mSocketHelper.sendEvent("new message", resTalker.getOutMessage());
//                            break;
//                        case HexTalker.CODE_LIVE_LOCTATION:
//                            logMgr.d(TAG, "case LIVE LOCATION");
//                            break;
//                    }
//                    break;
//                case HEX_MESSAGE_RESPONSE:
//                    break;
//            }
//        }
//    };

    // Hex Message Handler
    static class HexHandler extends Handler {
        WeakReference<MainActivity> mParent;

        public HexHandler(WeakReference<MainActivity> parent) {
            mParent = parent;
        }

        @Override
        public void handleMessage(Message msg) {
            MainActivity parent = mParent.get();

            logMgr.d(TAG, "entered hexHandler");
            logMgr.d(TAG, "msg.what: " + String.valueOf(msg.what));
            logMgr.d(TAG, "msg.arg1: " + String.valueOf(msg.arg1));
            switch (msg.what) {
                case HEX_MESSAGE_REQUEST:
                    switch (msg.arg1) {
                        case HexTalker.CODE_LAST_LOCTATION:
                            logMgr.d(TAG, "case LAST LOCATION");
                            // 1. get last known location
                            Location mLastLocation = mHexLocation.getLastLocation();
                            if (mLastLocation == null) {
                                logMgr.e(TAG, "mLastLocation == null");
                                break;
                            }
                            // 2. prepare the message: Last Location
                            HexTalker resTalker = new HexTalker(parent.getApplicationContext());
                            resTalker.setType(HexTalker.TYPE_RESPONSE);
                            resTalker.setCode(HexTalker.CODE_LAST_LOCTATION);
                            Bundle b = new Bundle();
                            b.putString(
                                    ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE,
                                    String.valueOf(mLastLocation.getLatitude()) );
                            b.putString(
                                    ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE,
                                    String.valueOf(mLastLocation.getLongitude()) );
                            resTalker.setExtra(HexTalker.EXTRA_LOCATION, b);

                            resTalker.formatOutMessage();
                            // 3. send the message back to client
                            mSocketHelper.sendEvent("new message", resTalker.getOutMessage());
                            break;

                        case HexTalker.CODE_LIVE_LOCTATION:
                            if(msg.arg2 == HexTalker.TRACKING_STATE_ON
                                    && !mHexLocation.mRequestingLocationUpdates) {
                                mHexLocation.mRequestingLocationUpdates = true;
                                mHexLocation.startLocationUpdates();
                            }
                            else if (msg.arg2 == HexTalker.TRACKING_STATE_OFF
                                    && mHexLocation.mRequestingLocationUpdates) {
                                mHexLocation.mRequestingLocationUpdates = false;
                                mHexLocation.stopLocationUpdates();
                            }
                            break;
                        case HexTalker.CODE_NAV:
                            if(msg.arg2 == HexTalker.NAV_FORWARD)
                                try {
                                    mRobotBTManager.write(fwdBtReady);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            if(msg.arg2 == HexTalker.NAV_BACKWARD)
                                try {
                                    mRobotBTManager.write(bwdBtReady);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            if(msg.arg2 == HexTalker.NAV_LEFT)
                                try {
                                    mRobotBTManager.write(leftBtReady);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            if(msg.arg2 == HexTalker.NAV_RIGHT)
                                try {
                                    mRobotBTManager.write(rightBtReady);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            break;
                        case HexTalker.CODE_AP_ON:
                            if (!HexAutoPilot.active){
                                mHexLocation.startLocationUpdates();
                                logMgr.d(TAG, "AUTOPILOT ENABLED");
                                HexTalker apTalker = (HexTalker) msg.obj;
                                mHexAp = new HexAutoPilot(
                                        apTalker.getWayPointStartLatLon(),
                                        apTalker.getWayPointEndLatLon());
                                mHexAp.setEnabled(true);
                            }
                            break;
                        case HexTalker.CODE_AP_OFF:
                            logMgr.d(TAG, "AUTOPILOT DISABLED");
                            break;
                    }
                    break;
//                case HEX_MESSAGE_RESPONSE:
//                    switch (msg.arg1) {
//                        case HexTalker.CODE_LAST_LOCTATION:
//                            logMgr.d(TAG, "=== HERE 1 ===");
//                            LatLng coor = (LatLng) msg.obj;
//                            logMgr.d(TAG, "handler > lat: " + coor.latitude);
//                            logMgr.d(TAG, "handler > lon: " + coor.longitude);
//                            MapActivity.updateMap(
//                                    MapActivity.MAP_CODE_UPDATE_HEX_MARKER,
//                                    (LatLng) msg.obj);
//                            break;
//                    }
//                    break;
            }
        }
    }

    // Bluetooth Handler
    // The Handler that gets information back from the RobotBluetoothManager
    private final Handler btHandler = new Handler(){

        @Override
        public void handleMessage(Message msg){
            switch (msg.what){
                case BT_MESSAGE_STATE_CHANGE:
                    switch(msg.arg1) {
                        case RobotBluetoothManager.STATE_CONNECTED:
                            logMgr.w(TAG, "Already connected to: " + mBtConnectedDeviceName);
                            break;
                        case RobotBluetoothManager.STATE_CONNECTING:
                            logMgr.i(TAG, "Attempting to connect to: " + mBtConnectedDeviceName);
                            break;
                        case RobotBluetoothManager.STATE_LISTEN:
                            break;
                        case RobotBluetoothManager.STATE_NONE:
                            logMgr.w(TAG, "Not connected to any BT device");
                            break;
                    }
                    break;
                case BT_MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from a buffer
                    String writeMessage = new String(writeBuf);
                    logMgr.d(TAG, "Sending:" + writeMessage);
                    break;
                case BT_MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    MainActivity.logMgr.d(TAG, "(2) buffer length: " + readBuf.length);
                    String readMessage = new String(readBuf, 0, msg.arg1);

                    // Notify outgoingCommandHandler about the incoming response from robot
//				outgoingCommandHandler.responseReceived(readBuf);

                    logMgr.d(TAG, "Received:" + readMessage);
                    break;
                case BT_MESSAGE_DEVICE_NAME:
                    mBtConnectedDeviceName = msg.getData().getString(BT_DEVICE_NAME);
                    logMgr.i(TAG, "Now Connected to: " + mBtConnectedDeviceName);
            }
        }
    };

    /**
     * @author Sahand
     * @param requestCode The code which figures out from which activity/for what reason this Intent is received
     * @param resultCode Indicates different behaviors towards handling the incoming Intent
     * @param data The actual data (including possible EXTRA material) coming with the Intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        logMgr.d(TAG, "Entered onActivityResult()");
        switch(requestCode){
            case REQUEST_ENABLE_BT:
                // Request to enable Bluetooth has returned
                if(resultCode == Activity.RESULT_OK){
                    // Bluetooth is now enabled
                    logMgr.d(TAG, "Bluetooth turned on by user");
                } else {
                    btnBluetooth.setChecked(false);
                    logMgr.e(TAG, "Bluetooth is not enabled, error occured...");
                }
                break;
            case REQUEST_CONNECT_DEVICE:
                // Coming back from BluetoothDeviceList with a device to connect
                if(resultCode == Activity.RESULT_OK){
                    // Get the device MAC address
                    logMgr.d(TAG, "entered REQUEST_CONNECT_DEVICE");
                    String address = data.getExtras().getString(BluetoothDeviceList.EXTRA_DEVICE_ADDRESS);

                    // Get the BluetoothDevice object
                    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);

                    // Now the fun part! Connect to the device
                    logMgr.i(TAG, "Conneting to: " + address);
                    mRobotBTManager.connect(device);

                }
                else if(resultCode == Activity.RESULT_CANCELED){
                    // 1. User hit the back button
                    // 2. BluetoothDeviceList activity faced error
                    logMgr.e(TAG, "Coming back with nothing!");
                }
        }
    }

    protected void prepareBluetoothCommands() {
        String forward = ApplicationConstants.msg_forward;
        String[] fwdDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        fwdDuration[0] = DURATION;
        Command fwdCommand = new Command(forward, fwdDuration);
        fwdBtReady = fwdCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String backward = ApplicationConstants.msg_backward;
        String[] backwardDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        backwardDuration[0] = DURATION;
        Command bwdCommand = new Command(backward, backwardDuration);
        bwdBtReady = bwdCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String left = ApplicationConstants.msg_left;
        String[] leftDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        leftDuration[0] = DURATION;
        Command leftCommand = new Command(left, leftDuration);
        leftBtReady = leftCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String right = ApplicationConstants.msg_right;
        String[] rightDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        rightDuration[0] = DURATION;
        Command rightCommand = new Command(right, rightDuration);
        rightBtReady = rightCommand.computeAndReturnBluetoothCommand(getApplicationContext());
    }

    /**
     * Use this method to display any messages on the log text view on the main screen
     * @param message The formatted message to be displayed
     */
    public static synchronized void writeToTextView(final String message) {
        mLogTextView.post(new Runnable() {
            @Override
            public void run() {
                mLogTextView.append(message);
            }
        });
    }
}
