package com.android.tum.de.hexbotcontroller.comm;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.android.tum.de.hexbotcontroller.controller.ApplicationConstants;
import com.android.tum.de.hexbotcontroller.controller.MainActivity;

import com.android.tum.de.hexbotcontroller.R;
import com.google.android.gms.maps.model.LatLng;

/**
 * @author Sahand
 * @version 1.0.0
 * implements the necessary message protocols for communication between Client and Controller
 * creates the outgoing messages, as well as parsing incoming ones and inform different modules
 * according to the specified aciton
 */
public class HexTalker {
    private static final String TAG = "HexTalker";

    public static final int TYPE_REQUEST = 110;
    public static final int TYPE_RESPONSE = 120;

    public static final int CODE_LAST_LOCTATION = 210;
    public static final int CODE_LIVE_LOCTATION = 220;
    public static final int CODE_NAV = 230;
    public static final int CODE_AP_ON = 240;
    public static final int CODE_AP_OFF = 241;

    public static final int EXTRA_LOCATION = 310;
    public static final int EXTRA_TRACKING_STATE = 320;
    public static final int EXTRA_NAV = 330;
    public static final int EXTRA_AP_COURSE = 340;

    public static final int TRACKING_STATE_ON = 321;
    public static final int TRACKING_STATE_OFF = 322;
    public static final int TRACKING_DATA = 323;
    public static final int NAV_FORWARD = 331;
    public static final int NAV_BACKWARD = 332;
    public static final int NAV_LEFT = 333;
    public static final int NAV_RIGHT = 334;

    private Handler mHandler;

    private String message;
    private String inMessage;
    private StringBuilder outMessage = new StringBuilder();
    private String[] splittedMessage;
    private final Context mContext;
    private double mLatitude;
    private double mLongitude;
    private double mWayPointStartLat;
    private double mWayPointStartLon;
    private double mWayPointEndLat;
    private double mWayPointEndLon;

    private HexResponse mHexResponse;
    private HexRequest mHexRequest;


    // ============================
    private String messageType;
    private String messageCode;
    private String messageExtra;
    // ============================

    public HexTalker(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
        mHexRequest = new HexRequest();
        mHexResponse = new HexResponse();
        outMessage.append(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);

        inMessage = null;
    }

    public HexTalker(Context context) {
        mContext = context;

        mHexRequest = new HexRequest();
        mHexResponse = new HexResponse();

        inMessage = null;

        outMessage.append(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
    }

    public boolean initializeParser() {
        inMessage = inMessage.trim();
        splittedMessage = inMessage.split(ApplicationConstants.TALKER_DELIMITER);
        return splittedMessage.length != 0;
    }

    public boolean parseIncomingMessage() {
        // 1. validate IDENTIFIER
        if( !validateInMessageIdentifier() ) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong identifier");
            return false;
        }

        // 2. validate TYPE
        if(!validateInMessageType()) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong type");
            return false;
        }
        // 3. validate CODE
        // 4. get EXTRA
        if(!validateInMessageCode()) {
            MainActivity.logMgr.e(TAG, "invalid message: wrong code");
            return false;
        }
        return true;
    }

    private boolean validateInMessageIdentifier() {
        // Omit leading and trailing whitespace
        return splittedMessage[0].equalsIgnoreCase(ApplicationConstants.TALKER_PROTOCOL_IDENTIFIER);
    }

    private boolean validateInMessageType() {
        splittedMessage[1] = splittedMessage[1].toLowerCase();
        switch (splittedMessage[1]) {
            case ApplicationConstants.TALKER_TYPE_REQUEST:
                mHexRequest.isTypeSet = true;
                return true;
            case ApplicationConstants.TALKER_TYPE_RESPONSE:
                mHexResponse.isTypeSet = true;
                return true;
            case ApplicationConstants.TALKER_TYPE_COMMAND:
                // For the moment we treat Command as a type of Request from Client
                // TODO add COMMAND type as a class
                mHexRequest.isTypeSet = true;
                return true;
        }
        return false;
    }

    private boolean validateInMessageCode() {
        if(mHexRequest.isTypeSet) {
            switch (splittedMessage[2]) {
                case ApplicationConstants.TALKER_CODE_LAST_LOCATION:
                    MainActivity.logMgr.d(TAG, "request: last location");
                    Message msg = mHandler.obtainMessage(
                            MainActivity.HEX_MESSAGE_REQUEST,
                            CODE_LAST_LOCTATION,
                            -1);
                    mHandler.sendMessage(msg);
                    return true;

                case ApplicationConstants.TALKER_CODE_LIVE_LOCATION:
                    MainActivity.logMgr.d(TAG, "request: LIVE LOCATION");
                    parseExtra(EXTRA_TRACKING_STATE);
                    return true;
                case ApplicationConstants.TALKER_CODE_NAV:
                    MainActivity.logMgr.d(TAG, "request: NAV");
                    parseExtra(EXTRA_NAV);
                    return true;
                case ApplicationConstants.TALKER_CODE_AP_ON:
                    MainActivity.logMgr.d(TAG, "request: AP_ON");
                    parseExtra(EXTRA_AP_COURSE);
                    Message apOnMsg = mHandler.obtainMessage(
                            MainActivity.HEX_MESSAGE_REQUEST,
                            CODE_AP_ON,
                            -1,
                            this);
                    mHandler.sendMessage(apOnMsg);
                    return true;
                case ApplicationConstants.TALKER_CODE_AP_OFF:
                    MainActivity.logMgr.d(TAG, "reqest: AP_OFF");
                    Message apOffMessage = mHandler.obtainMessage(
                            MainActivity.HEX_MESSAGE_REQUEST,
                            CODE_AP_OFF,
                            -1);
                    mHandler.sendMessage(apOffMessage);
                    return true;
            }
        }
        if(mHexResponse.isTypeSet) {
            // do nothing for now...
            // to simplify for the moment, we don't have any message of type RESPONSE coming
            // from the Client app
        }
        return false;
    }

    private void parseExtra(int extraType) {
        // TODO: error handling
        if(extraType == EXTRA_LOCATION) {
            String[] coordinate = splittedMessage[3].split(ApplicationConstants.TALKER_PARAM_DELIMITER);
            mLatitude = Double.parseDouble(coordinate[0]);
            mLongitude = Double.parseDouble(coordinate[1]);
        } else if (extraType == EXTRA_TRACKING_STATE) {
            // ASSUMPTION:
            // client always receives LIVE_LOCATION requests with extra "ON"/"OFF"
            // method parseExtra is different in Client and Controller codes
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_LIVE_LOCATION_ON)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_LIVE_LOCTATION,
                        TRACKING_STATE_ON);
                mHandler.sendMessage(msg);
                return;
            }
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_LIVE_LOCATION_OFF)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_LIVE_LOCTATION,
                        TRACKING_STATE_OFF);
                mHandler.sendMessage(msg);
                return;
            }
        } else if (extraType == EXTRA_NAV) {
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_NAV_FORWARD)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_NAV,
                        NAV_FORWARD);
                mHandler.sendMessage(msg);
                return;
            }
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_NAV_BACKWARD)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_NAV,
                        NAV_BACKWARD);
                mHandler.sendMessage(msg);
                return;
            }
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_NAV_LEFT)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_NAV,
                        NAV_LEFT);
                mHandler.sendMessage(msg);
                return;
            }
            if(splittedMessage[3].equalsIgnoreCase(ApplicationConstants.TALKER_EXTRA_NAV_RIGHT)) {
                Message msg = mHandler.obtainMessage(
                        MainActivity.HEX_MESSAGE_REQUEST,
                        CODE_NAV,
                        NAV_RIGHT);
                mHandler.sendMessage(msg);
            }

        } else if (extraType == EXTRA_AP_COURSE) {
            String[] wayPointCoor = splittedMessage[3].split(ApplicationConstants.TALKER_PARAM_DELIMITER);
            mWayPointStartLat = Double.parseDouble(wayPointCoor[0]);
            mWayPointStartLon = Double.parseDouble(wayPointCoor[1]);
            mWayPointEndLat = Double.parseDouble(wayPointCoor[2]);
            mWayPointEndLon = Double.parseDouble(wayPointCoor[3]);
        }
    }

    public void setInMessage(String msg) {
        if (msg == null)
            MainActivity.logMgr.w(TAG, mContext.getString(R.string.talker_empty_message_warn));
        inMessage = msg;
    }

    public void setType(int type) {
        switch (type) {
            case TYPE_REQUEST:
                messageType = ApplicationConstants.TALKER_TYPE_REQUEST;
                break;
            case TYPE_RESPONSE:
                messageType = ApplicationConstants.TALKER_TYPE_RESPONSE;
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message type");
                break;
        }
    }

    public void setCode(int code) {
        switch (code) {
            case CODE_LAST_LOCTATION:
                messageCode = ApplicationConstants.TALKER_CODE_LAST_LOCATION;
                break;
            case CODE_LIVE_LOCTATION:
                messageCode = ApplicationConstants.TALKER_CODE_LIVE_LOCATION;
                break;
            default:
                MainActivity.logMgr.e(TAG, "invalid message code");
                break;
        }
    }

    public void setExtra(int extraType, Bundle data) {
        String lat, lon;
        switch (extraType) {
            case EXTRA_LOCATION:
                lat = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE);
                lon = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE);
                formatExtra(lat,lon);
                break;
            case EXTRA_TRACKING_STATE:
                // for now, the RESPONSE to LIVE_LOCATION is always the lat + lon
                lat = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE);
                lon = data.getString(ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE);
                formatExtra(lat,lon);
                break;
        }
    }

    private void formatExtra(String lat, String lon) {
        messageExtra = lat + ApplicationConstants.TALKER_PARAM_DELIMITER + lon;
    }

    public void formatOutMessage() {
        outMessage.append(messageType);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
        outMessage.append(messageCode);
        outMessage.append(ApplicationConstants.TALKER_DELIMITER);
        outMessage.append(messageExtra);

        MainActivity.logMgr.d(TAG, "outMessage = " + outMessage);
    }

    public String getOutMessage() {
        if(outMessage != null)
            return outMessage.toString();
        return mContext.getString(R.string.talker_empty_message_warn);
    }

    public HexRequest getRequest() {
        return this.mHexRequest;
    }

    public HexResponse getResponse() {
        return this.mHexResponse;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public LatLng getWayPointStartLatLon() {
        return new LatLng(mWayPointStartLat, mWayPointStartLon);
    }

    public LatLng getWayPointEndLatLon(){
        return new LatLng(mWayPointEndLat, mWayPointEndLon);
    }

    // PRIVATE SUB CLASSES
    private class HexRequest {
        private String mCode;
        private String mExtra;
        private boolean isTypeSet;
        private boolean isCodeSet;

        public HexRequest() {
            mCode = "empty";
            mExtra = "empty";
            isTypeSet = false;
            isCodeSet = false;
        }

        private String getCode() {
            return mCode;
        }

        private String getExtra() {
            return mExtra;
        }

        private void setCode(String code) {
            mCode = code;
        }

        private void setExtra(String extra) {
            mExtra = extra;
        }
    }

    private class HexResponse {
        private String mCode;
        private String mExtra;
        private boolean isTypeSet;
        private boolean isCodeSet;


        public HexResponse() {
            mCode = "empty";
            mExtra = "empty";
            isTypeSet = false;
            isCodeSet = false;
        }

        private String getCode() {
            return mCode;
        }

        private String getExtra() {
            return mExtra;
        }

        private void setCode(String code) {
            mCode = code;
        }

        private void setExtra(String extra) {
            mExtra = extra;
        }
    }

}
