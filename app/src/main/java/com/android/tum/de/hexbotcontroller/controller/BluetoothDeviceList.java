package com.android.tum.de.hexbotcontroller.controller;

import java.util.Set;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.tum.de.hexbotcontroller.R;

/**
 * @author Sahand
 * This activity provides the user with a user interface to scan for nearby devices and choose one
 * to connect to.
 * There are two available lists:
 * 1. Paired devices,
 * 2. Newly discovered devices,
 *
 * For phase 1, User can choose only one device to communicate with.
 */
public class BluetoothDeviceList extends ActionBarActivity {
    private static final String TAG = "RobotController-BTDeviceList";

    public static String EXTRA_DEVICE_ADDRESS = "device_address";

    // Member fields
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayAdapter<String> mPairedDevicesAdapter;
    private ArrayAdapter<String> mNewDevicesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        // In case the user hit the back button, set the result
        setResult(Activity.RESULT_CANCELED);

        Button scanBtn = (Button)findViewById(R.id.btnScan);
        scanBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                manageDiscovery();

            }
        });

        // Initialize array adapters:
        // 1. Previously paired devices
        // 2. Newly discovered devices
        mPairedDevicesAdapter = new ArrayAdapter<String>(this, R.layout.device_item);
        mNewDevicesAdapter = new ArrayAdapter<String>(this, R.layout.device_item);

        // ListView for paired devices
        ListView pairedListView = (ListView)findViewById(R.id.pairedDevicesListView);
        pairedListView.setAdapter(mPairedDevicesAdapter);
        pairedListView.setOnItemClickListener(mDeviceListClickListener);

        // ListView for newly discovered devices
        ListView newDevicesListView = (ListView)findViewById(R.id.newDeviceListView);
        newDevicesListView.setAdapter(mNewDevicesAdapter);
        newDevicesListView.setOnItemClickListener(mDeviceListClickListener);

        // Register for broadcasts when a bluetooth device is discovered
        IntentFilter discoveredFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mBtDeviceReceiver, discoveredFilter);

        // Register for broadcasts with DISCOVERY_FINISHED intent
        IntentFilter finishedDiscoveryFilter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(mBtDeviceReceiver, finishedDiscoveryFilter);

        // Get BluetoothAdapter instance
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Get a set of currently paired devices
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        // Look for previously paired devices
        if(pairedDevices.size() > 0){
            // Add each of them to ArrayAdapter
            for(BluetoothDevice device : pairedDevices){
                mPairedDevicesAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        } /*else {
			mPairedDevicesAdapter.add("No Paired Device");
		}*/
    }

    @Override
    protected void onStop(){
        super.onStop();

        if(mBluetoothAdapter != null){
            mBluetoothAdapter.cancelDiscovery();
        }

        unregisterReceiver(mBtDeviceReceiver);
    }

    /**
     * @author Sahand
     * BroadcastReceiver to get discovered devices
     */
    private final BroadcastReceiver mBtDeviceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(BluetoothDevice.ACTION_FOUND.equals(action)){
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if(device.getBondState() != BluetoothDevice.BOND_BONDED){
                    mNewDevicesAdapter.add(device.getName() + "\n" + device.getAddress());
                }
            }

        }
    };

    /**
     * @author Sahand
     * Start device discovery. Search for Bluetooth devices
     */
    private void manageDiscovery(){

        // Check whether Bluetooth is enabled.
        if(!mBluetoothAdapter.isEnabled()){
            Toast.makeText(BluetoothDeviceList.this, R.string.alert_bt_is_off, Toast.LENGTH_LONG).show();
        }
        // Scan button can either work as a turn on or turn of discovery.
        // If we're already in Discovering mode, turn off the discovery.
        else if(mBluetoothAdapter.isDiscovering()){
            mBluetoothAdapter.cancelDiscovery();
        }

        mBluetoothAdapter.startDiscovery();
    }

    private OnItemClickListener mDeviceListClickListener = new OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            MainActivity.logMgr.d(TAG, "Entered onItemClick()");
            // First cancel discovery to increase performance
            mBluetoothAdapter.cancelDiscovery();

            // Get the device MAC address
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);
            MainActivity.logMgr.d(TAG, "Address extracted:" + address);

            // Create the result intent and include the MAC address
            Intent intent = new Intent();
            intent.putExtra(EXTRA_DEVICE_ADDRESS, address);
            MainActivity.logMgr.d(TAG, "EXTRA ready!");

            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

}
