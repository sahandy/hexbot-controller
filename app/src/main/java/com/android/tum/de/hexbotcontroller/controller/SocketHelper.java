package com.android.tum.de.hexbotcontroller.controller;

import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.socketio.client.IO;

import java.net.URISyntaxException;

/**
 * Handles connection to the server
 * Creates a socket and maintains a constant connection to the server
 */
public class SocketHelper {

    private static final String TAG = "SocketHelper";

    private static SocketHelper instance = null;
    Socket socket;

    protected SocketHelper() {
        // Exists only to defeat instantiation
    }

    public static SocketHelper getInstance() {
        if(instance == null) {
            instance = new SocketHelper();
        }
        return instance;
    }

    public boolean connect(String server){
        Log.d(TAG, "entered connect");
        try {
            socket = IO.socket(server);
            socket.connect();
            return true;

        } catch (URISyntaxException e) {
            e.printStackTrace();
            Log.d("test",e.getMessage());
            return false;
        }
    }

    public boolean disconnect() {
        try {
            socket.disconnect();
            return true;
        } catch (Exception e) {
            MainActivity.logMgr.e(TAG, "error disconnecting");
            e.printStackTrace();
            return false;
        }
    }

    public void addListener(String eventName,Emitter.Listener listener){
        socket.on(eventName,listener);
    }

    public void sendEvent(String eventName, Object data){
        socket.emit(eventName,data);
    }
}
