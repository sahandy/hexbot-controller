package com.android.tum.de.hexbotcontroller.controller;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.tum.de.hexbotcontroller.R;
import com.android.tum.de.hexbotcontroller.log.LogLevel;

/**
 * @author Sahand, Alex
 * This PreferenceFragment is responsible to create and maintain the preferences which are accessible by the user.
 * Instead of using an XML file, all preferences is programmatically inserted into an instance of SharedPreferences interface
 * 		Resaon: Defining different log levels in a static XML file is tricky. This way, we can handle this preference more effectively.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final String TAG = "SettingsFragment";

    private static final int KEY_COM_DURATION = R.string.pref_command_exec_duration_key;
    private static final int DEFAULT_COM_DURATION = R.string.pref_command_exec_duration_default;
    private static final int SUMM_COM_DURATION = R.string.pref_command_exec_duration_summ;
    private static final int KEY_RESTORE_DEFAULT = R.string.pref_restore_defaults_key;

    // Duty Cycle Preferences: Keys, Resource IDs
    private static final int LM_FWD = R.string.pref_duty_cycle_left_forward_key;
    private static final int RM_FWD = R.string.pref_duty_cycle_right_forward_key;
    private static final int LM_BWD = R.string.pref_duty_cycle_left_backward_key;
    private static final int RM_BWD = R.string.pref_duty_cycle_right_backward_key;
    private static final int LM_TL = R.string.pref_duty_cycle_left_leftTurn_key;
    private static final int RM_TL = R.string.pref_duty_cycle_right_leftTurn_key;
    private static final int LM_TR = R.string.pref_duty_cycle_left_rightTurn_key;
    private static final int RM_TR = R.string.pref_duty_cycle_right_rightTurn_key;

    // Duty Cycle Preferences: Defaults, Resource IDs
    private static final int LM_FWD_D = R.string.pref_duty_cycle_left_forward_default;
    private static final int RM_FWD_D = R.string.pref_duty_cycle_right_forward_default;
    private static final int LM_BWD_D = R.string.pref_duty_cycle_left_backward_default;
    private static final int RM_BWD_D = R.string.pref_duty_cycle_right_backward_default;
    private static final int LM_TL_D = R.string.pref_duty_cycle_left_leftTurn_default;
    private static final int RM_TL_D = R.string.pref_duty_cycle_right_leftTurn_default;
    private static final int LM_TR_D = R.string.pref_duty_cycle_left_rightTurn_default;
    private static final int RM_TR_D = R.string.pref_duty_cycle_right_rightTurn_default;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
       /*
        *  UI Initialization
        */
        setDynamicSummaries();

        // setup button for "Restore Defaults"
        Preference pref = (Preference) findPreference("keyPrefRestoreDefaults");
        pref.setOnPreferenceClickListener(
                new Preference.OnPreferenceClickListener() {

                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        resetToDefaults(preference);
                        setDynamicSummaries();
                        return false;
                    }
                });

    }


    @Override
    public void onResume(){
        super.onResume();


        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        boolean dutyCycleConvertFlag = true;

        if (getResources().getString(R.string.pref_logLevel_key).equals(key)){
            ListPreference listPref = (ListPreference) pref;
            listPref.setSummary(listPref.getEntry());
            //Change the trace level of the CustomLogManager object
            MainActivity.logMgr.setTraceLevel( LogLevel.valueOf(listPref.getValue()) );
            MainActivity.logMgr.i(TAG, "Log level is changed to: " + listPref.getValue());
        }

        if (getResources().getString(R.string.pref_command_exec_duration_key).equals(key)){
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int currentDuration = 0;
            EditTextPreference editTextPref = (EditTextPreference) pref;

            String regularExp = "\\d+"; // one or more digits

            // input error check: check whether the input contains ONLY digits
            if(editTextPref.getText().matches(regularExp)) {
                // input is a valid number, now check the range
                currentDuration = Integer.parseInt(editTextPref.getText());
                if( currentDuration<1 || currentDuration>120){ // TODO: Hardcoded values
                    // input is not within the valid range - reset to default
                    Toast.makeText(getActivity(), "Please enter a valid number between 1 and 120", Toast.LENGTH_SHORT).show();
                    currentDuration = Integer.parseInt(getResources().getString(DEFAULT_COM_DURATION));
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(getResources().getString(KEY_COM_DURATION), getResources().getString(DEFAULT_COM_DURATION) );
                    editor.commit();
                    editTextPref.setText( sharedPrefs.getString( getResources().getString(KEY_COM_DURATION), getResources().getString(DEFAULT_COM_DURATION) ) );
                }
            } else {
                // input contained something rather than digits - reset to default
                Toast.makeText(getActivity(), "Please enter a valid number between 1 and 120", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(getResources().getString(KEY_COM_DURATION), getResources().getString(DEFAULT_COM_DURATION) );
                editor.commit();
                editTextPref.setText( sharedPrefs.getString( getResources().getString(KEY_COM_DURATION), getResources().getString(DEFAULT_COM_DURATION) ) );
            } // end of input error check

        }

        // Max duty cycle for motors
        if (
                getResources().getString(R.string.pref_duty_cycle_left_forward_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_right_forward_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_left_backward_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_right_backward_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_left_leftTurn_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_right_leftTurn_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_left_rightTurn_key).equals(key) ||
                        getResources().getString(R.string.pref_duty_cycle_right_rightTurn_key).equals(key)
                )
        {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int currentSetting = 0;
            EditTextPreference editTextPref = (EditTextPreference) pref;

            String regularExp = "\\d+"; // one or more digits

            // input error check: check whether the input contains ONLY digits
            if(editTextPref.getText().matches(regularExp)) {
                // input is a valid number, now check the range
                currentSetting = Integer.parseInt(editTextPref.getText());
                if(currentSetting<=0 || currentSetting>100){ // TODO: Hardcoded values
                    // input is not within the valid range - reset to default
                    Toast.makeText(getActivity(), "Please enter a valid number between 0 and 100", Toast.LENGTH_SHORT).show();
                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, getDefaultValue(key) );
                    editor.commit();
                    editTextPref.setText(sharedPrefs.getString(key, ApplicationConstants.defaultMaxDutyCycle));

                    dutyCycleConvertFlag = false;
                }
            } else {
                // input contained something rather than digits
                Toast.makeText(getActivity(), "Please enter a valid number between 0 and 100", Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(key, getDefaultValue(key) );
                editor.commit();
                editTextPref.setText(sharedPrefs.getString(key, ApplicationConstants.defaultMaxDutyCycle));

                dutyCycleConvertFlag = false;
            } // end of input error check

            if(dutyCycleConvertFlag) {
                // Input is valid. Convert to binary string
                if(getResources().getString(R.string.pref_duty_cycle_left_forward_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "00000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_right_forward_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "00000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_left_backward_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "10000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_right_backward_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "10000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_left_leftTurn_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "10000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_right_leftTurn_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "00000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_left_rightTurn_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "00000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
                if(getResources().getString(R.string.pref_duty_cycle_right_rightTurn_key).equals(key))
                {
                    String output = editTextPref.getText();
                    String binarizedOutput = Integer.toBinaryString( Integer.parseInt(output) );
                    String filler = "10000000";
                    int len = binarizedOutput.length();
                    binarizedOutput = filler.substring(0, 8-len).concat(binarizedOutput);

                    SharedPreferences.Editor editor = sharedPrefs.edit();
                    editor.putString(key, binarizedOutput);
                    editor.commit();
                }
            }
        }
        setDynamicSummaries();
    }

    /**
     * Some of the preferences do not have constant strings as a summary. Instead, they fetch the actual preferences values and put them in the summary.
     * This method sets the summary of such preferences dynamically.
     */
    private void setDynamicSummaries() {

        SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
        String currentDuration = prefs.getString( getResources().getString(KEY_COM_DURATION), getResources().getString(DEFAULT_COM_DURATION) );
        /**
         *  command exec. duration
         */
        EditTextPreference execDurationPref = (EditTextPreference)findPreference( getResources().getString(R.string.pref_command_exec_duration_key) );
        execDurationPref.setSummary(getResources().getString( SUMM_COM_DURATION, Integer.parseInt(currentDuration) ));
        execDurationPref.getEditText().setSelectAllOnFocus(true);

        /**
         * Max Duty Cycles
         */
        EditTextPreference dutyCyclePref;
        // Left Motor > Forward
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(LM_FWD));
        dutyCyclePref.setSummary( decimalRepresentation(LM_FWD, prefs, LM_FWD_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Right Motor > Forward
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(RM_FWD));
        dutyCyclePref.setSummary( decimalRepresentation(RM_FWD, prefs, RM_FWD_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Left Motor > Backward
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(LM_BWD));
        dutyCyclePref.setSummary( decimalRepresentation(LM_BWD, prefs, LM_BWD_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Right Motor > Backward
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(RM_BWD));
        dutyCyclePref.setSummary( decimalRepresentation(RM_BWD, prefs, RM_BWD_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Left Motor > Turn Left
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(LM_TL));
        dutyCyclePref.setSummary( decimalRepresentation(LM_TL, prefs, LM_TL_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Right Motor > Turn Left
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(RM_TL));
        dutyCyclePref.setSummary( decimalRepresentation(RM_TL, prefs, RM_TL_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Left Motor > Turn Right
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(LM_TR));
        dutyCyclePref.setSummary( decimalRepresentation(LM_TR, prefs, LM_TR_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
        // Right Motor > Turn Right
        dutyCyclePref = (EditTextPreference)findPreference(getResources().getString(RM_TR));
        dutyCyclePref.setSummary( decimalRepresentation(RM_TR, prefs, RM_TR_D) );
        dutyCyclePref.getEditText().setSelectAllOnFocus(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.logMgr.d(TAG, "Entered SettingsFragmetn.onCreateView()");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedState) {
        MainActivity.logMgr.d(TAG, getClass().getSimpleName() + ":entered onActivityCreated()");
        super.onActivityCreated(savedState);
    }

    public void resetToDefaults(Preference preference) {
        String key = preference.getKey();
        if ( key.equals(getResources().getString(KEY_RESTORE_DEFAULT)) ) {
            PreferenceManager
                    .getDefaultSharedPreferences(getActivity())
                    .edit()
                    .clear()
                    .commit();
            //PreferenceManager.setDefaultValues(getActivity(), R.xml.preferences, true);
            refreshFragment();
        }
    }


    private void refreshFragment() {
        getFragmentManager()
                .beginTransaction()
                .remove(getFragmentManager()
                        .findFragmentByTag(SettingsActivity.SETTING_FRAGMENT_TAG))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsFragment(), SettingsActivity.SETTING_FRAGMENT_TAG)
                .commit();
        Toast.makeText(getActivity(), "All attributes are set to default", Toast.LENGTH_SHORT).show();
    }

    private String getDefaultValue(String key) {
        if( key.equals(getResources().getString(LM_FWD)) )
            return getResources().getString(LM_FWD_D);

        if( key.equals(getResources().getString(RM_FWD)) )
            return getResources().getString(RM_FWD_D);

        if( key.equals(getResources().getString(LM_BWD)) )
            return getResources().getString(LM_BWD_D);

        if( key.equals(getResources().getString(RM_BWD)) )
            return getResources().getString(RM_BWD_D);

        if( key.equals(getResources().getString(LM_TL)) )
            return getResources().getString(LM_TL_D);

        if( key.equals(getResources().getString(RM_TL)) )
            return getResources().getString(RM_TL_D);

        if( key.equals(getResources().getString(LM_TR)) )
            return getResources().getString(LM_TR_D);

        if( key.equals(getResources().getString(RM_TR)) )
            return getResources().getString(RM_TR_D);

        return null;
    }
    private CharSequence decimalRepresentation(int ResID, SharedPreferences prefs, int defVal) {
        String binarizedString = prefs.getString(getResources().getString(ResID), getResources().getString(defVal));
        // remove the character at index zero; "12345678" --> "2345678"
        binarizedString = binarizedString.substring(1, binarizedString.length());
        binarizedString = Integer.toString( Integer.parseInt(binarizedString, 2) );

        return (CharSequence) binarizedString;
    }

}
