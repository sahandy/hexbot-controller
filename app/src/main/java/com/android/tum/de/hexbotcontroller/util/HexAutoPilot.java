package com.android.tum.de.hexbotcontroller.util;

import android.content.Context;
import android.location.Location;

import com.android.tum.de.hexbotcontroller.controller.ApplicationConstants;
import com.android.tum.de.hexbotcontroller.controller.Command;
import com.android.tum.de.hexbotcontroller.controller.MainActivity;
import com.google.android.gms.maps.model.LatLng;

/**
 * @author Sahand
 * @version 1.0.0
 * This class is responsible for handling Autonomous Navigation Tasks
 * Gets start and end point and tries to move the robot to closest point possible to
 *          1. Start Point, and then towards
 *          2. End Point
 */
public class HexAutoPilot {
    private static final String TAG = "HexAutoPilot";

    // ACTIVE mode -> use HexSensor for realtime orientation data
    // PASSIVE mode -> use lastlocation bearing.
    public static final int MODE_ACTIVE = 1;
    public static final int MODE_PASSIVE = 0;

    Context mContext;
    /*
    TEMP VALUES FOR BLUETOOTH NAVIGATION COMMANDS
     TODO handle bluetooth command preparation in a separate class
     */
    protected static Byte [] fwdBtReady, bwdBtReady, leftBtReady, rightBtReady;
    private final String DURATION = ApplicationConstants.defaultCommandDuration;

    private AutoDriveThread mAutoDriveThread;

    private float mHexHeading;
    private float mPathBearing = 0;
    private float mDeflection;
    private float mDistance;

    private Location mOrigin = new Location ("Origin");
    private Location mDestination = new Location ("Destination");

    public static boolean active;

    public HexAutoPilot(LatLng orig, LatLng dest) {

        mOrigin.setLatitude(orig.latitude);
        mOrigin.setLongitude(orig.longitude);
        mDestination.setLatitude(dest.latitude);
        mDestination.setLongitude(dest.longitude);

        mPathBearing = mOrigin.bearingTo(mDestination);
        mDistance = mOrigin.distanceTo(mDestination);

        active = false;
    }

    public void setEnabled(boolean state) {
        active = state;
        if (active) {
            if(mAutoDriveThread == null)
                mAutoDriveThread = new AutoDriveThread();
            mAutoDriveThread.start();
        } else {
            if(mAutoDriveThread != null)
                mAutoDriveThread.cancel();
        }
    }

    private float getPathBearing() {
        return mPathBearing;
    }

    private boolean hasReachedOrigin() {
        Location currentLocation = MainActivity.mHexLocation.getLastLocation();
        float distance = currentLocation.distanceTo(mOrigin);
        return distance < 5;
    }

    private class AutoDriveThread extends Thread {

        public AutoDriveThread() {

        }

        public void run(){
            MainActivity.mHexSensor.registerSensorListener();
            while(!hasReachedOrigin() && active) {
                // ENABLE AUTOPILOT
                MainActivity.logMgr.d(TAG, "Waiting for 5 seconds... for Nothing!");

                // Temporary workaround: wait for 3 seconds.
                // Sensor data will become stable
                // TODO change the approach to 'moving average'
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHexHeading = MainActivity.mHexSensor.deviceHeading;

                mDeflection = (360 + ((mPathBearing + 360) % 360) - mHexHeading) % 360;

                // if 0 < mDeflection <= 180  TURN RIGHT
                // AS of NOW: every TURN_RIGHT command --> apprx. 30~45 degrees
                if (0 < mDeflection && mDeflection < 181) {
                    MainActivity.logMgr.d(TAG, "Turning right");
                    try {
                        MainActivity.mRobotBTManager.write(MainActivity.rightBtReady);
                    } catch (Exception e) {
                        cancel();
                        e.printStackTrace();
                        continue;
                    }
                } else { // if 181 < mDeflection < 359  TURN LEFT
                    MainActivity.logMgr.d(TAG, "Turning left");
                    try {
                        MainActivity.mRobotBTManager.write(MainActivity.leftBtReady);
                    } catch (Exception e) {
                        cancel();
                        e.printStackTrace();
                        continue;
                    }
                }
                // Wait for the robot to perform the command
                try {
                    Thread.sleep(Integer.valueOf(ApplicationConstants.defaultCommandDuration) * 1000 );
                } catch (InterruptedException e) {
                    cancel();
                    e.printStackTrace();
                }


                try {
                    MainActivity.mRobotBTManager.write(MainActivity.fwdBtReady);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
                // Wait for the robot to perform the command
                try {
                    Thread.sleep(Integer.valueOf(ApplicationConstants.defaultCommandDuration) * 1000 );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            MainActivity.mHexSensor.unregisterSensorListener();
            MainActivity.logMgr.d(TAG, "Thread: out of While Loop");
            active = false;
        }

        public void cancel() {
            active = false;
        }
    }
}