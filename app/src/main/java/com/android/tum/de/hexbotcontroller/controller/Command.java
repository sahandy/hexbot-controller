package com.android.tum.de.hexbotcontroller.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.io.UnsupportedEncodingException;
import java.util.BitSet;

import com.android.tum.de.hexbotcontroller.R;
import com.android.tum.de.hexbotcontroller.interfaces.CommandInterface;

/**
 * @author Alexander
 * Imported into Android Studio by Sahand on 2/20/2015.
 *
 */
public class Command implements CommandInterface{

    /**
     * Used for debugging and logging purposes.
     */
    public static final String TAG = "Command";
    /**
     * Store the command type
     * Can never be null.
     */
    private String command;
    /**
     * Store the parameters of the command type.
     * Can be null.
     */
    private String[] parameters;

    /**
     * Constructor
     * @param newCommand The command type to be executed against the robot.
     * @param newParameters The corresponding parameters of the command type. E.g. definition of the duration.
     */
    public Command(String newCommand, String[] newParameters) {
        if (newCommand == null) {
            throw new NullPointerException();
        }
        command = newCommand;
        parameters = newParameters;
        MainActivity.logMgr.d(TAG, "Creating new command with parameters: command: "+ command + "parameter[0]: " + (parameters == null?"null":parameters[0]));
    }
    /**
     * Use this method only for debugging purposes. It creates a String in the format: %commandType%%-parameters*%
     * Where the values between the % will be replaced by their values.
     * Example: forward-50
     */
    public String toString () {
        StringBuilder sb = new StringBuilder();
        sb.append(this.command);

        for(int i=0; i<this.parameters.length; i++) {
            sb.append("-");
            sb.append(parameters[i]);
        }
        return sb.toString();
    }

    /* (non-Javadoc)
     * @see de.tum.android.interfaces.CommandInterface#getCommand()
     */
    @Override
    public Byte[] computeAndReturnBluetoothCommand(Context context) {
        Byte[] byteCommand = new BluetoothMessage(this.command, this.parameters, context).getBluetoothMessage();

        //Added for debugging purposes only
        checkByteArray(byteCommand);
        // Can be null in case of errors. Therefore null handling has to be performed by the components which invoke this method.
        return byteCommand;
    }

    /**
     * Converts the Byte array into a Bitset and displays the result in the log.
     * @param input The Byte array to be processed.
     */
    private void checkByteArray(Byte[] input) {
        if(input == null) {
            return;
        }

        //copied from: http://stackoverflow.com/questions/6197411/converting-from-bitset-to-byte-array
        BitSet bits = new BitSet();
        for (int i = 0; i < input.length * 8; i++) {
            if ((input[input.length - i / 8 - 1] & (1 << (i % 8))) > 0) {
                bits.set(i);
            }
        }
        MainActivity.logMgr.d(TAG, bits.toString());
    }

    /**
     * The private class BluetoothMessage is responsible for setting bits to each of the single Byte objects.
     * It also computes the default values, in case faulty or missing parameters were provided.
     * @author Alexander
     *
     */
    private class BluetoothMessage {
        private Byte startSeq;
        private Byte leftMotor;
        private Byte rightMotor;
        private Byte duration;
        private Byte auxiliary;
        /**
         * This variable holds the overall status of the parsing process. If it is false something in the parsing process went wrong.
         */
        private boolean success = false;
        /**
         * Constructor
         * @param newCommand represents the command type taken from the originating SMS message.
         * @param newParameters For Release 1 maximum one parameter for each command. Could be extended to more parameters.
         * @param context The Context object is needed to access the SharedPreferences.
         */
        private BluetoothMessage (String newCommand, String[] newParameters, Context context) {
            MainActivity.logMgr.d(TAG, "Start creation of Bluetooth protocol message.");
            if(newCommand == null){
                MainActivity.logMgr.e(TAG, "The command type is null!");
                return;
            }
            String mDuration = calculateOrGetDefaultDuration(newParameters, context);
            //Defined constants per definition of the Bluetooth protocol
            String constStartSeq = ApplicationConstants.bluetoothCommand_startSequence;
            String constAuxiliary = ApplicationConstants.bluetoothCommand_auxiliary;

            // Commonly set parts of the Bluetooth message, independent from command type
            success = setStartSeq(constStartSeq);
            success = setDuration(mDuration);
            success = setAuxiliary(constAuxiliary);

            // Set parts of the Bluetooth message, which are dependent from the command type
            switch(newCommand) {
                case ApplicationConstants.msg_forward:
                    success = setLeftMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_left_forward_key), ApplicationConstants.bluetoothCommand_leftMotor_forward, context));
                    success = setRightMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_right_forward_key), ApplicationConstants.bluetoothCommand_leftMotor_forward, context));
                    break;
                case ApplicationConstants.msg_backward:
                    success = setLeftMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_left_backward_key), ApplicationConstants.bluetoothCommand_leftMotor_backward, context));
                    success = setRightMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_right_backward_key), ApplicationConstants.bluetoothCommand_rightMotor_backward, context));
                    break;
                case ApplicationConstants.msg_left:
                    success = setLeftMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_left_leftTurn_key), ApplicationConstants.bluetoothCommand_leftMotor_turnLeft, context));
                    success = setRightMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_right_leftTurn_key), ApplicationConstants.bluetoothCommand_rightMotor_turnLeft, context));
                    break;
                case ApplicationConstants.msg_right:
                    success = setLeftMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_left_rightTurn_key), ApplicationConstants.bluetoothCommand_leftMotor_turnRight, context));
                    success = setRightMotor(calculateOrGetDetaultMaxDutyCycle(context.getString(R.string.pref_duty_cycle_right_rightTurn_key), ApplicationConstants.bluetoothCommand_rightMotor_turnRight, context));
                    break;
                default:
                    MainActivity.logMgr.e(TAG, "Error: The following commandType was not recognized as valid: " + newCommand);
                    success = false;
                    return;
            }
        }

        /**
         * This method is responsible for calculating a Bit String (e.g. "10001100")
         * This Bit String is used to create the Bluetooth message parts.
         * @param key The key from the SharedPreferences
         * @param bitDefaultValue The default value, in case of errors receiving the SharedPreferences object
         * @param context Context is needed to access the SharedPreferences
         * @return Returns a String object, which contains at maximum 8 characters containing 1s and 0s
         */
        private String calculateOrGetDetaultMaxDutyCycle(String key, String bitDefaultValue, Context context) {
            //Validations
            if(key==null) {
                MainActivity.logMgr.w(TAG, "Key is missing for MaxDutyCycle value. Returning default value");
                return bitDefaultValue;
            }

            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String maxDutyCycle = sharedPrefs.getString(key, bitDefaultValue);
            //int tmp2 = Integer.valueOf(tmp1);

            return maxDutyCycle;
            //return Integer.toBinaryString(tmp2);
        }

        /**
         * This methods tries to calculate the bit representation of the provided parameters String array
         * In case of any errors, the specified default values of the SharePreferences will be used.
         * @param newParameters The String array, which holds all the parameters to be converted. For Release 1 only one single parameter for each command type will be allowed. Therefore only the first element of this array will be accessed.
         * @param context Context object to be able to access the SharePreferences.
         * @return
         */
        private String calculateOrGetDefaultDuration(String[] newParameters, Context context) {
            // Validations
            if(newParameters==null || newParameters.length==0) {
                MainActivity.logMgr.w(TAG, "Warning: No parameter provided for command type. Using default value.");
                return getDefaultParameterValue(context);
            }
            // Constraint: For Release 1 only one single parameter for each command type will be allowed!
            String parameterOneString = newParameters[0];
            // Validations
            if(parameterOneString==null
                    || parameterOneString.isEmpty()
                    || !parameterOneString.matches("[0-9]+")) {
                MainActivity.logMgr.w(TAG, "Warning: Invalid parameter provided for command type. Parameter value is: " + parameterOneString +" Using default value.");
                return getDefaultParameterValue(context);
            }

            // The duration will always be represented as an integer
            int parameterOneInt = Integer.valueOf(parameterOneString);
            // Check if the integer values falls within the boundaries 0...254
            if(parameterOneInt>ApplicationConstants.maxValueForParameter
                    || parameterOneInt < 0) {
                MainActivity.logMgr.w(TAG, "Warning: Invalid range provided for parameter. Parameter value is: " + parameterOneInt + ". Maximum value is: "+ ApplicationConstants.maxValueForParameter +" Using default value.");
                return getDefaultParameterValue(context);
            }
            MainActivity.logMgr.d(TAG, "The parameter value is: " + parameterOneInt);
            return Integer.toBinaryString(parameterOneInt);
        }

        /**
         * Access to SharedPreferences in order to receive the default value.
         * @param context Needed to have access to the SharedPreferences
         * @return Returns a binary representation of default value.
         */
        private String getDefaultParameterValue(Context context) {
            SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            String defaultDurationString = sharedPrefs.getString(context.getString(R.string.pref_command_exec_duration_key), ApplicationConstants.defaultCommandDuration);
            int defaultDurationInt = Integer.valueOf(defaultDurationString);
            return Integer.toBinaryString(defaultDurationInt);
        }

        /**
         * Computes the Byte array to be returned.
         * @return Returns a Byte Array in the order: startSeq, leftMotor, rightMotor, duration ,auxiliary. Returns Null in case of errors.
         */
        private Byte[] getBluetoothMessage() {
            if(!success){
                MainActivity.logMgr.e(TAG, "The conversion to Byte[] was not successful.");
                return null;
            }
            Byte[] result = {startSeq, leftMotor, rightMotor, duration ,auxiliary};
            return result;
        }

        /**
         * This method is used to set single bits within one Byte object.
         * @param bits
         * @return A Byte object containing a specified sequence of bits.
         */
        private Byte setBits(String bits) {
            //Validation
            try {
                if(bits==null
                        || bits.isEmpty()				// the bits may not be null
                        || bits.length() > 8			// one byte consists of maximum 8 bits
                        || !bits.matches("[10]+")) {	// only 1s and 0s are allowed
                    MainActivity.logMgr.e(TAG, "Error: The input bit String is faulty: " + bits);
                    return null;
                }
            } catch (Exception e1) {
                e1.printStackTrace();
                return null;
            }

            // If we want to convert the dual numbers in their integer representation
            // and from this integer representation we want to convert them into their
            // binary representation, then this means:
            // The bit sequence [11111111] will be converted into integer: 255
            // Integer 255 converted into byte array: [-1] (ISO-8859-1 encoding)
            byte result = 0;
            try {
                // Convert from bit array to integer, which is the ascii code
                int i = Integer.parseInt(bits, 2);
                // Get the ascii value based on that ascii code
                String asciiString = Character.valueOf((char)i).toString();
                // Get the byte value from that asciiString
                byte[] tmp = asciiString.getBytes("ISO-8859-1");
                // Validation
                if (tmp.length==0 || tmp.length>1){
                    MainActivity.logMgr.e(TAG, "Error:The returned byte array was bigger than 1 or empty! This may not be the case.");
                    throw new Exception("The returned byte array was bigger than 1! This may not be the case.");
                }
                result = tmp[0];
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                MainActivity.logMgr.e(TAG, "UnsupportedEncodingException was thrown.");
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                MainActivity.logMgr.e(TAG, "Generic Exception was thrown.");
                return null;
            }
            return result;
        }

        /**
         * Set the command part/Byte: startSeq
         * @param bits A binary String representation of the value to be set.
         * @return True, if Byte startSeq was computed successfully. False otherwise.
         */
        private boolean setStartSeq(String bits) {
            MainActivity.logMgr.d(TAG, "BitString for StartSeq: " + bits);
            Byte result = setBits(bits);
            if(result==null){
                MainActivity.logMgr.e(TAG, "There was an error setting the bits in the method setStartSeq(). The bit string is +" + bits);
                return false;
            }
            this.startSeq = result;
            return true;
        }

        /**
         * Set the command part/Byte: leftMotor
         * @param bits A binary String representation of the value to be set.
         * @return True, if Byte leftMotor was computed successfully. False otherwise.
         */
        private boolean setLeftMotor(String bits) {
            MainActivity.logMgr.d(TAG, "BitString for LeftMotor: " + bits);
            Byte result = setBits(bits);
            if(result==null){
                MainActivity.logMgr.e(TAG, "There was an error setting the bits in the method setLeftMotor(). The bit string is +" + bits);
                return false;
            }
            this.leftMotor = result;
            return true;
        }

        /**
         * Set the command part/Byte: rightMotor
         * @param bits A binary String representation of the value to be set.
         * @return True, if Byte rightMotor was computed successfully. False otherwise.
         */
        private boolean setRightMotor(String bits) {
            MainActivity.logMgr.d(TAG, "BitString for RigthMotor: " + bits);
            Byte result = setBits(bits);
            if(result==null){
                MainActivity.logMgr.e(TAG, "There was an error setting the bits in the method setRightMotor(). The bit string is +" + bits);
                return false;
            }
            this.rightMotor = result;
            return true;

        }

        /**
         * Set the command part/Byte: duration
         * @param bits A binary String representation of the value to be set.
         * @return True, if Byte duration was computed successfully. False otherwise.
         */
        private boolean setDuration(String bits) {
            MainActivity.logMgr.d(TAG, "BitString for Duration: " + bits);
            Byte result = setBits(bits);
            if(result==null){
                MainActivity.logMgr.e(TAG, "There was an error setting the bits in the method setDuration(). The bit string is +" + bits);
                return false;
            }
            this.duration = result;
            return true;
        }

        /**
         * Set the command part/Byte: auxiliary
         * @param bits A binary String representation of the value to be set.
         * @return True, if Byte auxiliary was computed successfully. False otherwise.
         */
        private boolean setAuxiliary(String bits) {
            MainActivity.logMgr.d(TAG, "BitString for Auxiliary: " + bits);
            Byte result = setBits(bits);
            if(result==null){
                MainActivity.logMgr.e(TAG, "There was an error setting the bits in the method setAuxiliary(). The bit string is +" + bits);
                return false;
            }
            this.auxiliary = result;
            return true;
        }
    }
}
