package com.android.tum.de.hexbotcontroller.controller;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.os.Handler;

import com.android.tum.de.hexbotcontroller.R;
import com.android.tum.de.hexbotcontroller.comm.RobotBluetoothManager;

/**
 * @author Sahand
 * FOR TESTING PURPOSES
 * Directly control the robot through the Controller app
 * Uses the Bluetooth connection to send the corresponding navigation commands to the robot
 */
public class VirtualJoyStickActivity extends ActionBarActivity {

    // Debugging
    public static final String TAG = "VirtualJoyStickActivity";

    private Button fwdBtn, bwdBtn, leftBtn, rightBtn;
    protected static Byte [] fwdBtReady, bwdBtReady, leftBtReady, rightBtReady;

    private final String DURATION = ApplicationConstants.defaultCommandDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_virtual_joystick);

        fwdBtn = (Button) findViewById(R.id.btnForward);
        bwdBtn = (Button) findViewById(R.id.btnBackward);
        leftBtn = (Button) findViewById(R.id.btnTurnLeft);
        rightBtn = (Button) findViewById(R.id.btnTurnRight);

        prepareBluetoothCommands();

        /*
        EVENT HANDLING FOR NAVIGATION BUTTONS
         */
        // FWD Button
        fwdBtn.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mActionFwd, 500);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mActionFwd);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mActionFwd = new Runnable() {
                @Override public void run() {
                    // Check if Bluetooth devices are connected
                    if(MainActivity.mRobotBTManager.getState() != RobotBluetoothManager.STATE_CONNECTED){
                        MainActivity.logMgr.e(TAG, "Bluetooth device is not connected.");
                    }
                    else{
                        // Write the Bluetooth command on the channel
                        try {
                            MainActivity.mRobotBTManager.write(VirtualJoyStickActivity.fwdBtReady);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Toast.makeText(getApplicationContext(), "emitting FWD", Toast.LENGTH_SHORT).show();
                    }
                    mHandler.postDelayed(this, 1000);
                }
            };
        });
        // BWD Button
        bwdBtn.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mActionBwd, 500);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mActionBwd);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mActionBwd = new Runnable() {
                @Override public void run() {
                    // Check if Bluetooth devices are connected
                    if(MainActivity.mRobotBTManager.getState() != RobotBluetoothManager.STATE_CONNECTED){
                        MainActivity.logMgr.e(TAG, "Bluetooth device is not connected.");
                    }
                    else{
                        // Write the Bluetooth command on the channel
                        try {
                            MainActivity.mRobotBTManager.write(VirtualJoyStickActivity.bwdBtReady);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Toast.makeText(getApplicationContext(), "emitting BWD", Toast.LENGTH_SHORT).show();
                    }
                    mHandler.postDelayed(this, 1000);
                }
            };
        });
        // LEFT Button
        leftBtn.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mActionLeft, 100);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mActionLeft);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mActionLeft = new Runnable() {
                @Override public void run() {
                    // Check if Bluetooth devices are connected
                    if(MainActivity.mRobotBTManager.getState() != RobotBluetoothManager.STATE_CONNECTED){
                        MainActivity.logMgr.e(TAG, "Bluetooth device is not connected.");
                    }
                    else{
                        // Write the Bluetooth command on the channel
                        try {
                            MainActivity.mRobotBTManager.write(VirtualJoyStickActivity.leftBtReady);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Toast.makeText(getApplicationContext(), "emitting LEFT", Toast.LENGTH_SHORT).show();
                    }
                    mHandler.postDelayed(this, 1000);
                }
            };
        });
        // RIGHT Button
        rightBtn.setOnTouchListener(new View.OnTouchListener() {
            private Handler mHandler;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (mHandler != null) return true;
                        mHandler = new Handler();
                        mHandler.postDelayed(mActionRight, 500);
                        break;
                    case MotionEvent.ACTION_UP:
                        if (mHandler == null) return true;
                        mHandler.removeCallbacks(mActionRight);
                        mHandler = null;
                        break;
                }
                return false;
            }

            Runnable mActionRight = new Runnable() {
                @Override public void run() {
                    // Check if Bluetooth devices are connected
                    if(MainActivity.mRobotBTManager.getState() != RobotBluetoothManager.STATE_CONNECTED){
                        MainActivity.logMgr.e(TAG, "Bluetooth device is not connected.");
                    }
                    else{
                        // Write the Bluetooth command on the channel
                        try {
                            MainActivity.mRobotBTManager.write(VirtualJoyStickActivity.rightBtReady);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Toast.makeText(getApplicationContext(), "emitting RIGHT", Toast.LENGTH_SHORT).show();
                    }
                    mHandler.postDelayed(this, 1000);
                }
            };
        });
    } // END onCreate

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void prepareBluetoothCommands() {
        String forward = ApplicationConstants.msg_forward;
        String[] fwdDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        fwdDuration[0] = DURATION;
        Command fwdCommand = new Command(forward, fwdDuration);
        fwdBtReady = fwdCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String backward = ApplicationConstants.msg_backward;
        String[] backwardDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        backwardDuration[0] = DURATION;
        Command bwdCommand = new Command(backward, backwardDuration);
        bwdBtReady = bwdCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String left = ApplicationConstants.msg_left;
        String[] leftDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        leftDuration[0] = DURATION;
        Command leftCommand = new Command(left, leftDuration);
        leftBtReady = leftCommand.computeAndReturnBluetoothCommand(getApplicationContext());

        String right = ApplicationConstants.msg_right;
        String[] rightDuration = new String[ApplicationConstants.maxNumberOfCommandParameters];
        rightDuration[0] = DURATION;
        Command rightCommand = new Command(right, rightDuration);
        rightBtReady = rightCommand.computeAndReturnBluetoothCommand(getApplicationContext());
    }
}
