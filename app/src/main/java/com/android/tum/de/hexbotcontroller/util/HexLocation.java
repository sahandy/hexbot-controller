package com.android.tum.de.hexbotcontroller.util;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.android.tum.de.hexbotcontroller.comm.HexTalker;
import com.android.tum.de.hexbotcontroller.controller.ApplicationConstants;
import com.android.tum.de.hexbotcontroller.controller.MainActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * @author Sahand
 * @version 1.0.0
 * Processes location-based information using GoogleApiClient
 * Implements location updates listener for constant location report
 */
public class HexLocation implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "HexLocation";

    private Context mContext;
    private GoogleApiClient mGoogleApiClient;

    private Location mLastLocation;
    private Location mCurrentLocation;

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    public boolean mRequestingLocationUpdates;

    public HexLocation(Context context) {
        mContext = context;
    }

    public void init() {
        MainActivity.logMgr.d(TAG, "entered init");

        mRequestingLocationUpdates = false;
        buildGoogleApiClient();
        if (!gpsStatus()) {
            Toast.makeText(mContext, "GPS is OFF!", Toast.LENGTH_SHORT).show();
            }
    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    private synchronized void buildGoogleApiClient() {
        MainActivity.logMgr.d(TAG, "entered buildGoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    /**
     * This function is called from the hosting Activity's onResume() method
     */
    public void connectGoogleApiClient() {
        if(mGoogleApiClient == null)
            buildGoogleApiClient();
        if(!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            MainActivity.logMgr.d(TAG, "Connected to GoogleApiClient");
        }
    }

    /**
     * This function is called from the hosting Activity's onStop() method
     */
    public void disconnectGoogleApiClient() {
        mRequestingLocationUpdates = false;
        stopLocationUpdates();
        if(mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            MainActivity.logMgr.d(TAG, "Disconnected from GoogleApiClient");
        }
    }

    /*
    * Helpers
    */
    public Location getLastLocation() {
//        if(mLastLocation != null)
//            return mLastLocation;
        // TODO error handling needed
        return mCurrentLocation;
    }


    private boolean gpsStatus() {
        final LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return true;
        return false;
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private  void createLocationRequest() {
        MainActivity.logMgr.d(TAG, "Entered createLocationRequest");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(2000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    public void startLocationUpdates() {
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        MainActivity.logMgr.d(TAG, "entered startLocationUpdates");

//        if(!mGoogleApiClient.isConnected()) {
//            MainActivity.logMgr.d(TAG, "mGoogleApiClient is not connected, attempting reconnect");
//            mGoogleApiClient.disconnect();
//            mGoogleApiClient.connect();
//        }

        if(!mGoogleApiClient.isConnected()) {
            MainActivity.logMgr.d(TAG, "mGoogleApiClient still not connected");
            return;
        }
        if(mLocationRequest == null)
            MainActivity.logMgr.d(TAG, "mLocationRequest is Null");

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, HexLocation.this);
        MainActivity.logMgr.d(TAG, "called API s requestLocationUpdates");
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    public void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.

        MainActivity.logMgr.d(TAG, "entered stopLocationUpdates");
        mRequestingLocationUpdates = false;
        if(!mGoogleApiClient.isConnected()) {
            MainActivity.logMgr.d(TAG, "mGoogleApiClient still not connected");
            return;
        }
        // The final argument to {@code requestLocationUpdates()} is a LocationListener
        // (http://developer.android.com/reference/com/google/android/gms/location/LocationListener.html).
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, HexLocation.this);
    }

    //
    // CALLBACKS
    //

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        MainActivity.logMgr.d(TAG, "Entered onConntected");
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mCurrentLocation = mLastLocation;

        // If request for LIVE_LOCATION comes before GoogleApiClient connects,
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    /**
     * Callback that fires when the location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        MainActivity.logMgr.d(TAG, "entered onLocationChanged");

        mCurrentLocation = location;

        // If the client fails to send the #ApplicationConstants.TALKER_EXTRA_LIVE_LOCATION_OFF
        // controller doesn't have to report location constantly
        if(!mRequestingLocationUpdates)
            return;
        HexTalker resHexTalker = new HexTalker(mContext);
        resHexTalker.setType(HexTalker.TYPE_RESPONSE);
        resHexTalker.setCode(HexTalker.CODE_LIVE_LOCTATION);
        Bundle b = new Bundle();
        b.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_LATITUDE,
                String.valueOf(mCurrentLocation.getLatitude()) );
        b.putString(
                ApplicationConstants.TALKER_BUNDLE_KEY_LONGITUDE,
                String.valueOf(mCurrentLocation.getLongitude()) );
        resHexTalker.setExtra(HexTalker.EXTRA_TRACKING_STATE, b);
        resHexTalker.formatOutMessage();

        MainActivity.logMgr.d(TAG, "sending: " + resHexTalker.getOutMessage());

        MainActivity.mSocketHelper.sendEvent("new message", resHexTalker.getOutMessage());
        //mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        //updateUI();
//        Toast.makeText(this, getResources().getString(R.string.location_updated_message),
//                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        MainActivity.logMgr.e(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        MainActivity.logMgr.w(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    /**
     * Updates fields based on data stored in the bundle.
     *
     * @param savedInstanceState The activity state saved in the Bundle.
     */
//    private void updateValuesFromBundle(Bundle savedInstanceState) {
//        Log.i(TAG, "Updating values from bundle");
//        if (savedInstanceState != null) {
//            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
//            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
//            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
//                mRequestingLocationUpdates = savedInstanceState.getBoolean(
//                        REQUESTING_LOCATION_UPDATES_KEY);
//                setButtonsEnabledState();
//            }
//
//            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
//            // correct latitude and longitude.
//            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
//                // Since LOCATION_KEY was found in the Bundle, we can be sure that mCurrentLocation
//                // is not null.
//                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
//            }
//
//            // Update the value of mLastUpdateTime from the Bundle and update the UI.
//            if (savedInstanceState.keySet().contains(LAST_UPDATED_TIME_STRING_KEY)) {
//                mLastUpdateTime = savedInstanceState.getString(LAST_UPDATED_TIME_STRING_KEY);
//            }
//            updateUI();
//        }
//    }

    /**
     * Stores activity data in the Bundle.
     */
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
//        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
//        savedInstanceState.putString(LAST_UPDATED_TIME_STRING_KEY, mLastUpdateTime);
//        super.onSaveInstanceState(savedInstanceState);
//    }
}
