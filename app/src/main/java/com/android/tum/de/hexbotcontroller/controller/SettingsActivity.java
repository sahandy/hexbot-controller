package com.android.tum.de.hexbotcontroller.controller;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * @author Sahand
 * Main class which holds the PreferenceFragment
 */
public class SettingsActivity extends PreferenceActivity {

    public static final String SETTING_FRAGMENT_TAG = "fragmentTag";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment(), SETTING_FRAGMENT_TAG)
                .commit();

    }
}
