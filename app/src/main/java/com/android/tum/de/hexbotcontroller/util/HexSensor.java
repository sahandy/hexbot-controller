package com.android.tum.de.hexbotcontroller.util;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.tum.de.hexbotcontroller.controller.MainActivity;

/**
 * @author Sahand
 * @version 1.0.0
 * Handles and processes the information coming from various sensors inside the device
 */
public class HexSensor implements SensorEventListener{
    private static final String TAG = "HexSensor";

    private static HexSensor mInstance = null;

    private Context mContext;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;

    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];

    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;

    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];

    protected float deviceHeading;

    /**
     * private constructor to make HexManager class singleton.
     * To get an instance of HexSensor, call HexSensor.getInstance
     */
    private HexSensor() {
        // empty...
    }

    /**
     * Keeps track of number of instances of HexSensor to
     * @return an instance of object HexSensor
     */
    public static HexSensor getInstance() {
        if(mInstance == null)
            mInstance = new HexSensor();
        return mInstance;
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void init() {
        // initialize your android device sensor capabilities
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }
    public void registerSensorListener(){
        mLastAccelerometerSet = false;
        mLastMagnetometerSet = false;
        if (mAccelerometer == null) {
            MainActivity.logMgr.d(TAG, "mAccelerometer is null");
        }
        if (mMagnetometer == null) {
            MainActivity.logMgr.d(TAG, "mMagnetometer is null");
        }
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterSensorListener() {
        mSensorManager.unregisterListener(this);
    }

    /*
    EVENT LISTENERS
     */
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor == mAccelerometer) {
            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
            mLastAccelerometerSet = true;
        } else if (event.sensor == mMagnetometer) {
            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
            mLastMagnetometerSet = true;
        }
        if (mLastAccelerometerSet && mLastMagnetometerSet) {
            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);

            deviceHeading = (int) Math.round(Math.toDegrees(mOrientation[0]));
        }

    }
}
