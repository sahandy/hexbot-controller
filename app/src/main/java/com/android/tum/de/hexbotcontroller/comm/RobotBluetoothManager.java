package com.android.tum.de.hexbotcontroller.comm;

/**
 * @author Sahand
 * This class is responsible for major communication between Android device and RN-41 module.
 * The main task is to initialize the connection between the two devices and send/receive commands/responses through Bluetooth channel.
 * Bluetooth commands come from OutgoingCommandHandler class.
 * ACK/NACK responses from the Robot will be sent back to OutgoingCommandHandler class.
 */

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.android.tum.de.hexbotcontroller.controller.ApplicationConstants;
import com.android.tum.de.hexbotcontroller.controller.MainActivity;


public class RobotBluetoothManager {
    // For debugging purpose
    private static final String TAG = "RobotBluetoothManager";

    // Name for the SDP record when creating server socket
    private static final String NAME = "RobotController";

    // Hard-coded UUID (for both devices)
    //private UUID DEFAULT_UUID = UUID.fromString(ApplicationConstants.UUID_android_device);
    private UUID DEFAULT_UUID = UUID.fromString(ApplicationConstants.UUID_generic);

    // Member fields
    private final BluetoothAdapter mBtAdapter;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;

    // Constants to indicate the current connection state
    public static final int STATE_NONE = 0; 		// no connection action is being performed
    public static final int STATE_LISTEN = 1;		// listening for incoming connections
    public static final int STATE_CONNECTING = 2;	// initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;	// connected to a remote device

    /**
     * Constructor.
     */
    public RobotBluetoothManager(Context context, Handler handler){
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
    }

    /**
     * Set method. Set the current connection state
     * @param state An integer showing the current connection state
     */
    private synchronized void setState(int state){
        mState = state;

        mHandler.obtainMessage(MainActivity.BT_MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
    }

    /**
     * Get method. Returns the current connection state
     */
    public synchronized int getState(){
        return mState;
    }

    /**
     * Start the RobotBluetoothManager. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     */
    public synchronized void start(){

        // Cancel any thread attempting to make a connection
        if(mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread which is currently running a connection
        if(mConnectedThread != null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to listen on a BluetoothServerSocket
        if(mAcceptThread == null){
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
        setState(STATE_LISTEN);
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device The BluetoothDevice to which we want to connect
     * @author Sahand
     */
    public synchronized void connect(BluetoothDevice device){

        MainActivity.logMgr.d(TAG, "Entered connect()");
        // Cancel any thread attempting to make a connection
        if(mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread which is currently running a connection
        if(mConnectedThread != null){
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect to the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    /**
     * Start the ConnectedThread to begin managing the Bluetooth connection
     * @param socket The BluetoothSocket on which the connection was made
     * @param device The BluetoothDevice that we are connected to
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device){

        MainActivity.logMgr.d(TAG, "entered connected()");

        // Cancel the thread that established the connection
        if(mConnectThread != null){
            mConnectThread.cancel();
            mConnectedThread = null;
        }

        // Cancel the accept thread
        // --- > NOTE: in PHASE 1.0, we would like to connect to only one device < ---
        if(mAcceptThread != null){
            mAcceptThread.cancel();
            mAcceptThread = null;
        }

        // Start the thread to manage connection
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI activity
        Message msg = mHandler.obtainMessage(MainActivity.BT_MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        MainActivity.logMgr.d(TAG, "device name: " + device.getName());
        bundle.putString(MainActivity.BT_DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    /**
     * Stop all threads
     */
    public synchronized void stop(){
        if(mConnectThread != null) { mConnectThread.cancel(); mConnectThread = null; }
        if(mConnectedThread != null) { mConnectedThread.cancel(); mConnectedThread = null; }
        if(mAcceptThread != null) { mAcceptThread.cancel(); mAcceptThread = null; }

        setState(STATE_NONE);
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out
     * @see ConnectedThread#write(byte[])
     * @author Sahand
     */
    public void write(Byte[] out) throws Exception {
        // Create temporary object
        ConnectedThread r;

        // Synchronize a copy of the ConnectedThread
        synchronized (this) {
            if(mState != STATE_CONNECTED) {
                MainActivity.logMgr.e(TAG, "not connected to any Bluetooth device");
                throw new Exception();
            }
            r = mConnectedThread;
        }

        // Attempt to write unsynchronized
        r.write(out);
    }

    public void write(byte[] out){
        Byte[] B = new Byte[out.length];
        for (int i = 0; i < out.length; i++) {
            B[i] = Byte.valueOf(out[i]);
        }
        try{
            write(B);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Indicate the connection attempt failure and notify the UI Activity
     */
    private void connectionFailed(){
        setState(STATE_LISTEN);

        MainActivity.logMgr.d(TAG, "Connecting to device failed!");
        // Send a failure message back to MainActivity
        Message msg = mHandler.obtainMessage(MainActivity.BT_MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.BT_TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    private void connectionLost(){
        setState(STATE_LISTEN);

        // Send a failure message back to MainActivity
        Message msg = mHandler.obtainMessage(MainActivity.BT_MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString(MainActivity.BT_TOAST, "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);
    }

    /**
     * This thread runs while listening for incoming connections. It behaves like a server-side client.
     * It runs until a connection is
     * 1. accepted,
     * 2. cancelled.
     * @author Sahand
     *
     */
    private class AcceptThread extends Thread{
        // The local server socket
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            BluetoothServerSocket btTempServerSocket = null;

            // Create a new listening server socket
            try {
                btTempServerSocket = mBtAdapter.listenUsingRfcommWithServiceRecord(NAME, DEFAULT_UUID);
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "listen() failed\n" + e.getMessage());
            }

            mmServerSocket = btTempServerSocket;
        }

        public void run(){
            setName("AcceptThread");
            BluetoothSocket socket = null;

            // Listen to the server socket if we're not connected
            while(mState != STATE_CONNECTED){
                try {
                    // Blocking call, only return on
                    // 1. successful connection
                    // 2. exception
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    MainActivity.logMgr.e(TAG, "accept() failed\n" + e.getMessage());
                    break;
                }

                // If a connection was accepted
                if(socket != null){
                    synchronized (RobotBluetoothManager.this) {
                        switch (mState) {
                            case STATE_LISTEN:
                            case STATE_CONNECTING:
                                // Situation normal. Start the connected thread
                                connected(socket, socket.getRemoteDevice());
                                break;

                            case STATE_NONE:
                            case STATE_CONNECTED:
                                // possibilities: 1. Not ready, 2. Already connected.
                                // Terminating new socket
                                try {
                                    socket.close();
                                } catch (IOException e) {
                                    MainActivity.logMgr.e(TAG, "Could not close unwanted socket\n" + e.getMessage());
                                }
                                break;
                        }

                    }
                }
            }
        }
        public void cancel(){
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "close() on server failed\n" + e.getMessage());
            }
        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection with a device.
     * The connection could succeed or fail.
     * @author Sahand
     *
     */
    private class ConnectThread extends Thread{
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device){

            MainActivity.logMgr.d(TAG, "entered ConnectThread() constructor");
            mmDevice = device;
            BluetoothSocket btTempSocket = null;

            //boolean fecthedUUIDWithSdp = mmDevice.fetchUuidsWithSdp();

            // Get a BluetoothSocket for a connection to the given device
            try {
                btTempSocket = device.createRfcommSocketToServiceRecord(DEFAULT_UUID);
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "create() failed\n" + e.getMessage());
            }

            mmSocket = btTempSocket;
        }

        public void run(){
            setName("ConnectThread");

            MainActivity.logMgr.d(TAG, "entered ConnectThread().run()");

            // Cancel discovery to speed up the connection
            mBtAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try
            {
                // Blocking call. Will return on
                // 1. successful connection
                // 2. exception

                MainActivity.logMgr.d(TAG, "attempt: mmSocket.connect()");
                mmSocket.connect();
                MainActivity.logMgr.d(TAG, "mmSocket.connect() OK!");
            }
            // Bluetooth stack implementation: rfc_comm leak problem for Android JellyBean and higher
            // temporary workaround: attempt to invoke the socket.connect() method after the first failure.
            // Causes open sockets to close and open again.
            catch (IOException e)
            {
                Method m = null;
                try
                {
                    m = mmDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
                }
                catch (NoSuchMethodException e1)
                {
                    e1.printStackTrace();
                }
                try
                {
                    BluetoothSocket tempSocket = (BluetoothSocket) m.invoke(mmDevice, 1);
                    tempSocket.connect();
                }
                catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IOException e2)
                {
                    e2.printStackTrace();
                    connectionFailed();
                }

                // Close the socket
                try {
                    mmSocket.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                    MainActivity.logMgr.e(TAG, "unable to close() socket during connection failure\n" + e2.getMessage());
                }

                // Start over the whole thing to restart listening mode
                RobotBluetoothManager.this.start();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (RobotBluetoothManager.this) {
                mConnectThread = null;
            }

            // Start the connected thread

            MainActivity.logMgr.d(TAG, "call connected(...)");
            connected(mmSocket, mmDevice);
        }

        public void cancel(){
            try {
                mmSocket.close();
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "close() of connect socket failed\n" + e.getMessage());
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It is responsible for handling all incoming and outgoing transmissions.
     * @author Sahand
     */
    private class ConnectedThread extends Thread{
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket){
            mmSocket = socket;
            InputStream tempInStream = null;
            OutputStream tempOutStream = null;

            // Get the BluetoothSocket input and output streams
            try {
                tempInStream = socket.getInputStream();
                tempOutStream = socket.getOutputStream();
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "temp sockets not created\n" + e.getMessage());
            }

            mmInStream = tempInStream;
            mmOutStream = tempOutStream;
        }

        public void run(){

            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while(true){
                try {
                    // Read from the InputStream
                    // store them in the byte array "buffer"
                    // store number of the bytes which is read from input stream in "bytes"
                    bytes = mmInStream.read(buffer);

                    // Send the obtained response message for further process
                    MainActivity.logMgr.d(TAG, "number of bytes received: [" + bytes + "]");
                    MainActivity.logMgr.d(TAG, "(1) buffer length: " + buffer.length);
                    StringBuilder sb = new StringBuilder();
                    for (int i=0; i<bytes; i++) {
                        sb.append((char)buffer[i]);
                    }
                    //MainActivity.logMgr.d(TAG, "received value is: [" + sb.toString() + "]");
                    mHandler.obtainMessage(MainActivity.BT_MESSAGE_READ, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    MainActivity.logMgr.e(TAG, "disconnected\n" + e.getMessage());
                    connectionLost();
                    break;
                }
            }
        }

        /**
         * Write to connected OutStream
         * @param bufferObjects the bytes to write
         * @author Sahand
         */
        public void write(Byte[] bufferObjects){
            try {
                int j=0;
                byte[] buffer = new byte[bufferObjects.length];
                for(Byte b: bufferObjects)
                    buffer[j++] = b.byteValue();
                mmOutStream.write(buffer);

                // Share the sent message with the UI Activity
                mHandler.obtainMessage(MainActivity.BT_MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "Exception during write\n"+ e.getMessage());
            }
        }

        public void cancel(){
            try {
                mmSocket.close();
            } catch (IOException e) {
                MainActivity.logMgr.e(TAG, "close() of connect socket failed\n" +  e.getMessage());
            }
        }
    }
}
