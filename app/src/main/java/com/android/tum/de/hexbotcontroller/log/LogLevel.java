package com.android.tum.de.hexbotcontroller.log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sahand on 2/25/2015.
 *
 * This enumeration class is used to define all possible log levels, which are used throughout the application
 * @author Alexander
 *
 */
public enum LogLevel {
    verbose(0),
    debug(1),
    info(2),
    warning(3),
    error(4);
    /**
     * The corresponding value of the LogLevel instance.
     */
    private int value;
    /**
     * For performance issues all values are stored in a hash map.
     */
    private static final Map<Integer,LogLevel> valuesByCode;

    static {
        valuesByCode = new HashMap<Integer,LogLevel>();
        for(LogLevel level : LogLevel.values()) {
            valuesByCode.put(level.value, level);
        }
    }
    /**
     * Use this method to get the log level by providing its integer value.
     * @param code The integer value of the code to be looked up.
     * @return The LogLevel which lies behind the integer value.
     */
    public static LogLevel lookupByCode(int code) {
        return valuesByCode.get(Integer.valueOf(code));
    }
    /**
     * Setter method for the value of the LogLevel instance
     * @param value
     */
    private LogLevel(int value) {
        this.value = value;
    }

    /**
     * Getter method for the value of the LogLevel instance
     * @return The corresponding value of the LogLevel instance.
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Provides an array of all enumeration names used in this enumeration class.
     * @param e The ClassName.class to be checked for enumerations
     * @return Provides an array of all enumeration names used in this enumeration class.
     */
    public static String[] getNames(Class<? extends Enum<?>> e) {
        return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").split(", ");
    }

    /**
     * Provides an array of all values used in this enumeration class
     * @return Provides an array of all values used in this enumeration class
     */
    public static String[] getValues() {
        int i = LogLevel.values().length;
        String[] result = new String[i];
        for(int j=0; j<i; j++) {
            result[j] = String.valueOf(j);
        }
        return result;
    }
}
