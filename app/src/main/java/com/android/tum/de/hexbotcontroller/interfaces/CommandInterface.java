package com.android.tum.de.hexbotcontroller.interfaces;

import android.content.Context;

/**
 * @author Alexander
 * This is a generic interface used to provide a common understanding of a command.
 * This interface will be used in Release 1.0 to parse SMS commands to this Command object.
 * This interface can also be used to transform messages from other sources, e.g. Bluetooth, WiFi direct etc.
 */
public interface CommandInterface {
    /**
     * @return Returns the Byte representation of the Command to be sent through Bluetooth. The length of this array is exactly 5.
     */
    public Byte[] computeAndReturnBluetoothCommand(Context context);
}
