package com.android.tum.de.hexbotcontroller.log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.os.Build;
import android.os.Environment;
import android.util.Log;

import com.android.tum.de.hexbotcontroller.controller.MainActivity;

/**
 * This class is used as a single point of logging for the whole application.
 * It logs both, to the LogCat console and to the TextView user interface in the application
 * imported into Android Studio by Sahand
 * @author Alexander
 *
 */
public class CustomLogManager {

    /**
     * Defines the minimum level of the logs to be stored.
     */
    private LogLevel traceLevel;
    /**
     * Used for debugging and logging purposes
     */
    private final static String applicationTAG = "RobotController";
    /**
     * The date format which will be used to display the time stamp within the user interface
     * Time stamp format is: YEAR-MONTH-DAY-(delimiter)-HOUR:MINUTE:SECOND:Millisecond(3 digits)TIMEZONE
     */
    private final String format = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    /**
     * Constructor
     * @param newTraceLevel Set the minimum level of log messages to be stored.
     */
    public CustomLogManager (LogLevel newTraceLevel){
        setTraceLevel(newTraceLevel);
    }
    /**
     * Use this method to change the minimum level of log messages to be stored.
     * @param newTraceLevel
     */
    public void setTraceLevel(LogLevel newTraceLevel){
        this.traceLevel = newTraceLevel;
    }
    /**
     * Use this method to display all the information about the Android device.
     */
    public void init() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n"+"********************");
        sb.append("\n"+"Starting new output");
        sb.append("\n"+"Date and Time: "+ new SimpleDateFormat(format, Locale.getDefault()).format(Calendar.getInstance().getTime()));
        sb.append("\n"+"Model: "+ Build.MODEL);
        sb.append("\n"+"Product: "+ Build.PRODUCT);
        sb.append("\n"+"Manufacturer: "+ Build.MANUFACTURER);
        sb.append("\n"+"Hardware: "+ Build.HARDWARE);
        sb.append("\n"+"Log trace level: "+ traceLevel.name());
        sb.append("\n"+"********************");

        MainActivity.writeToTextView(sb.toString());
    }
    /**
     * verbose
     * @param tag
     * @param message
     */
    public void v(String tag, String message) {
        if(LogLevel.verbose.getValue() >= this.traceLevel.getValue()) {
            writeLogItem(
                    new LogItem(
                            constructTag(tag),
                            message,
                            LogLevel.verbose,
                            Calendar.getInstance().getTime()
                    )
            );
            Log.v(constructTag(tag), message);
        }
    }
    /**
     * debug
     * @param tag
     * @param message
     */
    public void d(String tag, String message) {
        if(LogLevel.debug.getValue() >= this.traceLevel.getValue()) {
            writeLogItem(
                    new LogItem(
                            constructTag(tag),
                            message,
                            LogLevel.debug,
                            Calendar.getInstance().getTime()
                    )
            );
            Log.v(constructTag(tag), message);
        }
    }
    /**
     * info
     * @param tag
     * @param message
     */
    public void i(String tag, String message) {
        if(LogLevel.info.getValue() >= this.traceLevel.getValue()) {
            writeLogItem(
                    new LogItem(
                            constructTag(tag),
                            message,
                            LogLevel.info,
                            Calendar.getInstance().getTime()
                    )
            );
            Log.v(constructTag(tag), message);
        }
    }
    /**
     * warning
     * @param tag
     * @param message
     */
    public void w(String tag, String message) {
        if(LogLevel.warning.getValue() >= this.traceLevel.getValue()) {
            writeLogItem(
                    new LogItem(
                            constructTag(tag),
                            message,
                            LogLevel.warning,
                            Calendar.getInstance().getTime()
                    )
            );
            Log.v(constructTag(tag), message);
        }
    }
    /**
     * error
     * @param tag
     * @param message
     */
    public void e(String tag, String message) {
        if(LogLevel.error.getValue() >= this.traceLevel.getValue()) {
            writeLogItem(
                    new LogItem(
                            constructTag(tag),
                            message,
                            LogLevel.error,
                            Calendar.getInstance().getTime()
                    )
            );
            Log.v(constructTag(tag), message);
        }
    }
    /**
     * Display the information in the application's user interface
     * @param logItem
     */
    private void writeLogItem(LogItem logItem) {
        MainActivity.writeToTextView(logItem.toString());
    }
    /**
     * Construct the TAG which will be displayed in the LogCat. It concatenates the applicationTag and the TAG of the class which calls the LogManager
     * @param tag
     * @return
     */
    private String constructTag(String tag) {
        return applicationTAG + "." + tag;
    }

    /**
     * @author Sahand
     * Writes the current Log to a text file on SD card.
     * @param data string to be saved to a text file
     */
    public void writeLogToFileSD(String data){
        // Check if external storage is available for read and write
        String sdCardState = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(sdCardState)){
            // On external storage availability, attempt to create and write the de.tum.android.log file
            String currentTimeStamp = getCurrentTimeStamp();

            // Get the root path of SD Card
            File rootSdCard = Environment.getExternalStorageDirectory();
            // Add a new directory to root path
            File dir = new File(rootSdCard.getAbsolutePath() + "/robot controller/");
            // Create the directory (if not available)
            dir.mkdir();

            // Create the actual file we're about to write into.
            StringBuilder fileName = new StringBuilder();
            fileName.append("log_");
            fileName.append(currentTimeStamp);
            fileName.append(".txt");

            d(applicationTAG, "Log saved as: " + fileName);
            Log.d(applicationTAG, "Log saved as: " + fileName);

            File logFile = new File(dir, fileName.toString());
            try {
                FileOutputStream outStream = new FileOutputStream(logFile);
                outStream.write(data.getBytes());
                outStream.close();
            } catch (FileNotFoundException e1) {
                e(applicationTAG, "File is not found!");
                Log.e(applicationTAG, "File is not found!");
                e1.printStackTrace();
            } catch (IOException e2) {
                e(applicationTAG, "Can not write into the file!");
                Log.e(applicationTAG, "Can not write into the file!");
                e2.printStackTrace();
            }

        } else {
            e(applicationTAG, "SD Card not available.");
            Log.e(applicationTAG, "SD Card not available.");
        }
    }

    /**
     * @author Sahand
     * Get method. Gets the current time and date.
     * @return current time stamp in the following format: YearMonthDay-HourMinuteSecond
     */
    private String getCurrentTimeStamp() {
        return new SimpleDateFormat("yyyyMMdd-HHmmss", Locale.getDefault()).format(Calendar.getInstance().getTime());
    }

    /**
     * The private class LogItem is used to store specific information about one log entry
     * @author Alexander
     *
     */
    private class LogItem {
        /**
         * The time stamp when the entry was created
         */
        private Date dateTime;
        /**
         * The severity log level of the entry
         */
        private LogLevel logLevel;
        /**
         * Used to identify which application and which part of the application generated the log message
         */
        private String TAG;
        /**
         * The message to be logged
         */
        private String message;

        /**
         * Constructor
         * @param newTag Set the TAG
         * @param newMessage Set the message to be displayed
         * @param newLogLevel Define the severity log level
         * @param newDateTime The time stamp of the message
         */
        private LogItem(String newTag, String newMessage, LogLevel newLogLevel, Date newDateTime){
            this.dateTime = newDateTime;
            this.logLevel = newLogLevel;
            this.TAG = newTag;
            this.message = newMessage;
        }

        /**
         * Get the String representation of the Log entry
         * @return The String representation of the Log entry
         */
        public String toString() {
            StringBuilder sb = new StringBuilder();
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
            sb.append("\n" + sdf.format(dateTime));
            sb.append("\n" + "TAG: " + TAG);
            sb.append("\n" + logLevel.name() + ": " + message);
            sb.append("\n" + "--------------------");
            return sb.toString();
        }
    }
}
